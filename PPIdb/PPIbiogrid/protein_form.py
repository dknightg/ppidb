from django import forms

class Proteinsubmission(forms.Form):
	proteins    	=   forms.Field(required = False, widget = forms.Textarea, initial="31676,34272; 10962,10981")
	species			=	forms.ChoiceField(required= False, widget = forms.Select(), choices 	= ([('4932','Yeast'),('9606','Human')]))
	identifier		= 	forms.ChoiceField(required=True,widget=forms.Select(),choices = ([('BIOGRID','BioGrid'),('SWISSPROT','SwissProt'),('OFFICIAL_SYMBOL','Official Symbol'),('SYNONYM','Synonym'),('SYSTEMATIC_NAME','Systematic Name'),('ENTREZ_GENE','Entrez Gene'),
						('HPRD','HPRD'),('GENBANK_PROTEIN_ACCESSION','GenBank Protein Accession'),('GENBANK_PROTEIN_GI','GenBank Protein GI'),('HGNC','HGNC'),('GENBANK_GENOMIC_DNA_GI','GenBank Genomic DNA GI'),('GENBANK_DNA_GI','GenBank DNA GI'),
						('MIM','MIM'),('MIRBASE','MIRbase'),('GENBANK_DNA_ACCESSION','GenBank DNA Accession'),('GENBANK_GENOMIC_DNA_ACCESSION','Genbank Genomic DNA Accession'),('TREMBL','TREMBL'),('REFSEQ_GENOMIC_DNA_ACCESSION','RefSeq Genomic DNA Accession'),
						('ECOGENE','EcoGene'),('ENSEMBL','ENSEMBL'),('IMGT/GENE-DB','IMGT'),('REFSEQ_DNA_ACCESSION','RefSeq DNA Accesssion'),('REFSEQ_PROTEIN_ACCESSION','RefSeq Protein Accession'),('SGD','SGD'),('GENEDB','GeneDB'),
						('VECTORBASE','VectorBase'),('VEGA','VEGA')]))
	files			=	forms.FileField(required= False)
	format			= 	forms.ChoiceField(required=False,widget=forms.Select(),choices= ([('tab2','Tab2'),('osprey','Osprey'),('2col','Two Columns')]))

