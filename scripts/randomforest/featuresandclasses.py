from common.djangolib import *
import tpfn
from itertools import izip, combinations_with_replacement, islice
import numpy as np
import pylab
import subprocess
from random import randint, choice
import string

"""

This module contains a variety of functions that are involved in 
generating a random forests, and making predictions using those random 
forests.

"""


def countgoterms(go_slim_name):

    """Description 

    Generates a list of go ids from a .obo  file

    Arguments

    go_slim_name    The name of the go slim file in the "gomapper" 
                    folder in the go_id directory without the .obo 
                    file extension. (type=String)

    Returns:

    go_ids          A list of go_ids. (type=List)


    """
    path        =   os.path.join(data_dir, "gomapper/",go_slim_name+".obo")
    goslim_file  =   open(path)
    
    go_ids  =   []
    flag    =   False



    for line in goslim_file:
        if line.startswith("id:") and flag==True:
            go_ids.append(line.lstrip("id:").strip())
            flag=False

        if line.startswith("[Term]"):
            flag=True

    return go_ids



def feature_names_gen(interaction = True, disorder_names_= ['disembl-465'], **kwargs):
    
    
    """Description

    This function generates a list of strings of feature names. If the 
    feature names are for an interaction the feature names are 
    duplicated and appended with "a" or "b"

    Arguments

    interaction         A boolean describing whether the feature names 
                        represent an interaction or an individual
                        (type=Boolean) 

    disorder_names_     A list of the experimental method names to
                        include in the -feature list. (type=List)

    **kwargs            A list of further keyword arguments that can 
                        contain the following arguments:

    go_slim_name        The go_slim_name to be used to generate the 
                        feature names. (type=String)

    filter_for_sgd      A boolean determining whether the sgd_features 
                        table should be used to generate the feature
                        names. (type=Boolean)

    filter_for_disorder A boolean determining whether the 
                        disorder_names_ should be used to generate the 
                        feature names.(type=Boolean)

    Returns

    feature_names:      A list of feature names.(type=List)


    """

    #print kwargs
    #print disorder_names_

    sgd_feature_names=[]
    disorder_names = []

    if kwargs["go_slim_name"] !=None:
        go_slim_names    =   countgoterms(kwargs["go_slim_name"])


    else:
        go_slim_names    =   []



 
    if kwargs["filter_for_sgd"] == True:
        #print "yo"
        sgd_feature_names = ['sgd_features_gravy',
                             'sgd_features_weight',
                             'sgd_features_length',
                             'sgd_features_cai',
                             'sgd_features_fop']



    if kwargs["filter_for_disorder"] == True:
        #print "yo"
        disorder_names = disorder_names_

    # print sgd_feature_names
    # print disorder_names

    feature_names       =   go_slim_names + sgd_feature_names + disorder_names
    
    # print feature_names

    goslim1=[]
    goslim2=[]

    if interaction==True:
        for a in feature_names:
        
            goslim1.append(a+"_a")
            goslim2.append(a+"_b")

        feature_names = goslim1+goslim2

    #print feature_names

    return feature_names


def pop_database_interaction_confidence(taxon_id_, rf_filename, method_, **kwargs):

    """Description

    This function populates the rf_prediction table with interaction 
    confidences for proteins of a given taxonomical id and of a given 
    species.

    Arguments

    rf_filename         A string of the filename of the random forest 
                        that is used to predict the confidence of the 
                        interaction. (Type=String)

    taxon_id_           A string of the taxonomical id of the proteins
                        for whom the interaction is predicted.
                        (Type=String)

    method_             A string of tThe name of the experimental 
                        method for which the interaction confidences 
                        are generated. This is inserted into the 
                        database for later reference.(Type=String)

    **kwargs            A list of further keyword arguments that 
                        contains the arguments of the functions 
                        feature_names and get_features

    Returns

    None
    

    """

    def chunker(iterable, size):

        """Description

        This is a generator that splits up iterables into chunks of a 
        certain size in order to split up operations into batches.
        
        Arguments

        iterable    An iterable that is to be split into chunksize
                    (type=Iterable)

        size:       An integer which is the size of the chunks into 
                    which the iterable is split. (type=Integer)


        """

        it = iter(iterable)
        item = list(islice(it, size))
        while item:
            yield item
            item = list(islice(it, size))

    interactor_list = interactor.objects.filter(species_key__taxon_id = taxon_id_)

    interactor_pairs = combinations_with_replacement(interactor_list, 2)

    chunksize = 300000
    total_pairs = float(sum(range(interactor_list.count())))

    i = 0

    for interactor_pairs_chunk in chunker(interactor_pairs, chunksize):
        
        i += 1

        data = predict_pairs_rf(interactor_pairs_chunk, rf_filename, **kwargs)

        predictions = []

        for proteins, data in izip(interactor_pairs_chunk, data):


            predictions.append(rf_prediction(p_false             = data[0],
                                             p_true              = data[1],
                                             interactor_a_key    = proteins[0] ,
                                             interactor_b_key    = proteins[1] ,
                                             rf_name             = rf_filename,
                                             method              = method_
                                             ))
        for prediction_chunk in chunker(predictions, 10000):
            rf_prediction.objects.batch_insert(*prediction_chunk)
        
        print "Percent complete = {:2.2f}".format(float(chunksize)*i*100/float(total_pairs))

    return

def predict_pairs_rf(interactor_pairs, rf_filename, **kwargs):

    """Description

    This function predicts the confidences for protein pairs of a 
    contained within the interactor_pairs_list.

    Arguments

    interactor_pairs    A list of lists containing 2 interactor 
                        objects from the database. (Type=List)

    rf_filename         A string of the filename of the random forest 
                        that is used to predict the confidence of the 
                        interaction.(Type=String)


    **kwargs            A list of further keyword arguments that 
                        contains the arguments the functions 
                        feature_names and get_features

    Returns

    data                The calculated protein confidences. 
                        (Type=numpy.array)


    
    """

    print interactor_pairs

    feature_names       = feature_names_gen(**kwargs)

    #print feature_names

    features            = get_features(interactor_pairs, feature_names, **kwargs)

    features            = np.vstack((np.array(feature_names), features[0:]))

    random_fn           =   ''.join(choice(string.letters + string.digits) for i in xrange(10))
    r_inputfile         =   os.path.join("/tmp", random_fn)

    random_fn           =   ''.join(choice(string.letters + string.digits) for i in xrange(10))
    r_outputfile        =   os.path.join("/tmp", random_fn)
    np.savetxt(r_inputfile,  features,fmt="%s", delimiter=",")

    try:
        subprocess.check_call(["Rscript", 
                                     os.path.join(Rscript_dir, "predict.R"), 
                                     r_inputfile, 
                                     os.path.join(data_dir, "randomforest/", rf_filename), 
                                     r_outputfile ])

        data = np.genfromtxt(r_outputfile, dtype=float, delimiter=',', names=True)


    except Exception:
        raise 

    finally:
        os.remove(r_inputfile)
        os.remove(r_outputfile)

    return data



def get_features(interactor_pairs, feature_names, go_slim_name, disorder_names_ = ["disembl-465"], **kwargs):

    """Description

    This function returns a list of lists containing the values 
    of the features for the two interactors in each interactor 
    pair.

    Arguments

    interactor_pairs    A list of lists containing 2 interactor 
                        objects from the database. (type=List)


    feature_names       A list of feature_names. (type=List)

    go_slim_name        The name of the go slim file in the "gomapper" 
                        folder in the go_id directory of that name 
                        with a .obo file extension. (type=String)

    disorder_names_     A list of the method names to include in the 
                        feature list. (type=List)


    **kwargs            A list of further keyword arguments that 
                        contains the arguments:
                        

    filter_for_sgd      A boolean determining whether the sgd_features
                        table should be used to generate the feature 
                        names.(type=Boolean)

    filter_for_disorder A boolean determining whether the 
                        disorder_names_ should be used to generate 
                        the feature names. (type=Boolean)

    Returns

    features            A numpy array containing the values of the 
                        interacting proteins features.
                        (Type=numpy.array)

    
    """

    n = len(interactor_pairs)
    
    features = np.zeros((n,len(feature_names)))


    for i, pair in  enumerate(interactor_pairs):



        if len(pair) == 2:
            if randint(0,1) == True:
                a   =   pair[0]
                b   =   pair[1]

            else:
                a = pair[1]
                b = pair[0]


        elif len(pair) == 1:
            a   =   pair[0]
            b   =   pair[0]

        else:
            print "ERROR number of proiteins in interaction is neither 1 nor 2: {}".format(str(pair))

        

        go_ids_a    =   a.go_slim_set.filter(go_slim_type_m2m_key__go_slim_reference = go_slim_name).values_list("go_slim_id", flat = True)
        go_ids_b    =   b.go_slim_set.filter(go_slim_type_m2m_key__go_slim_reference = go_slim_name).values_list("go_slim_id", flat = True)


        if go_ids_a.count()!=0:
            for go_id_a in go_ids_a:
                
                j = feature_names.index(go_id_a+"_a")
                features[i][j] = 1


            for go_id_b in go_ids_b:            

                j = feature_names.index(go_id_b+"_b") 
                features[i][j] = 1

            
        if kwargs["filter_for_sgd"] ==True:

            sgds_a      =   a.sgd_features_set.values()[0]
            sgds_b      =   b.sgd_features_set.values()[0]

            sgd_feature_names = ['sgd_features_gravy',
                                 'sgd_features_weight',
                                 'sgd_features_length',
                                 'sgd_features_cai',
                                 'sgd_features_fop']

            for sgd_feature in sgd_feature_names:
                

                ja = feature_names.index(sgd_feature+"_a")

                features[i][ja] = sgds_a[sgd_feature]
               
                jb = feature_names.index(sgd_feature+"_b")
                features[i][jb] = sgds_b[sgd_feature]



        if kwargs["filter_for_disorder"] == True:
                        
            for disorder_name in disorder_names_:
                tallys_a       =   a.mobi_disorder_set.filter(mobi_disorder_method = disorder_name).values()[0]
                tallys_b       =   b.mobi_disorder_set.filter(mobi_disorder_method = disorder_name).values()[0]

                dis_a          =   float(tallys_a["mobi_disorder_Tally_One"])/(tallys_a["mobi_disorder_Tally_Zero"] + tallys_a["mobi_disorder_Tally_One"])
                dis_b          =   float(tallys_b["mobi_disorder_Tally_One"])/(tallys_b["mobi_disorder_Tally_Zero"] + tallys_b["mobi_disorder_Tally_One"])


                ja = feature_names.index(disorder_name+"_a")
                features[i][ja] = dis_a
           
                jb = feature_names.index(disorder_name+"_b") 
                features[i][jb] = dis_b

    return features

def generate_random_forest(go_slim, outputfiles_name, datasets, taxon_id, disorder_names_, sgd, dis):

    """Description 

    This function generates a random forest and stores it in the 
    randomforest folder in the data directory.

    Arguments 

    go_slim             The name of the go slim file in the "gomapper"
                        folder in the go_id directory of that name 
                        with a .obo file extension. (type=String)

    outputfiles_name    The name of the outputfile that the 
                        randomforest is saved under in the
                        randomforest folder in the data directory.
                        (type=String)

    disorder_names_     A list of the method names to include in the 
                        feature list. (type=List)

    taxon_id            A string of the taxonomical id of the proteins
                        for whom the interaction is predicted. 
                        (type=String)

    datasets            The datasets from the hc_interactions table 
                        which are to be used to generate the true 
                        positive set.(type=List)

    sgd                 A boolean determining whether the sgd_features
                        table should be used to generate the feature
                        names.(type=Boolean)

    dis                 A boolean determining whether the 
                        disorder_names_ should be used to generate the
                        feature names.(type=Boolean)

    Returns

    None

    """

    headers, features, labels= get_training_set_rf( datasets, 
                                                    taxon_id,
                                                    disorder_names = disorder_names_,
                                                    go_slim_name=go_slim, 
                                                    filter_for_sgd=sgd, 
                                                    filter_for_disorder=dis)
    
    features = np.vstack((np.array(headers), features[0:]))

    #print len(features)

    #print data_dir


    feature_file  = os.path.join(data_dir, "randomforest/",outputfiles_name+"_features")
    response_file = os.path.join(data_dir, "randomforest/",outputfiles_name+"_response")
    rf_outputfile = os.path.join(data_dir, "randomforest/",outputfiles_name+".rf")


    np.savetxt(feature_file,  features, fmt="%s", delimiter=",")
    np.savetxt(response_file, labels,   delimiter=",")



    subprocess.check_call(["Rscript", os.path.join(Rscript_dir,"rfgen.R"), 
                                                   "-r", response_file, 
                                                   "-p", feature_file,
                                                   "-o", rf_outputfile,
                                                   "-t", "0" ])
    return


def get_training_set_rf(datasets, taxon_id, disorder_names=['disembl-HL',"espritz-nmr"],**kwargs):

    """Description

    This function generates a training set for random forest with two 
    equally sized classes. One class is the interactions referenced by
    the hc_interactions table the second class is randomly generated 
    from protein pairs for the given species.


    Arguments 

    datasets            The datasets from the hc_interactions table 
                        which are to be used to generate the true 
                        positive set.

    taxon_id            A string of the taxonomical id of the proteins
                        for whom the interaction is predicted.

    disorder_names_     A list of the experimental methods for 
                        disorder to include in the feature list.

    **kwargs            A list of keyword arguments that contains 
                        the following keyword arguments

    filter_for_sgd      A boolean determining whether the sgd_features
                        table should be used to generate the feature 
                        names.

    filter_for_disorder A boolean determining whether the 
                        disorder_names_ should be used to generate the
                        feature names.

    Returns

    feature_names       The feature names of the columns in 
                        features.(Type=List)

    features            A numpy array containing the values of the 
                        interacting proteins features.
                        (Type=numpy.array)

    labels              The classification of the interactions (rows) 
                        in features.(Type=List)



    """

    feature_names       =   feature_names_gen(disorder_names_ = disorder_names, **kwargs)
    #print feature_names

    tp_interactors      =   tpfn.get_tp_results(datasets, **kwargs)
    
    rand_interactors    =   tpfn.get_n_tn_results(len(tp_interactors), taxon_id , **kwargs)

    labels = [1]*len(tp_interactors) + [0]*len(rand_interactors)
    
    interactor_pairs    =   tp_interactors + rand_interactors

    features = get_features(interactor_pairs, feature_names, disorder_names_=disorder_names, **kwargs)

    return feature_names, features, labels


if __name__ == '__main__':


    ##how to pop database with interaction confidences

    taxon_id        =   "4932"
    rf_filename     =   "y2h_gsd_sc.rf"
    go_slim         =   "goslim_yeast"
    method          =   "y2h"
    sgd            =   True
    dis            =   True

    pop_database_interaction_confidence(taxon_id,
                                        rf_filename,
                                        method,
                                        go_slim_name        =go_slim,
                                        disorder_names_     =     ['disembl-HL',"espritz-nmr"],
                                        filter_for_sgd      =sgd,
                                        filter_for_disorder =dis)


    # how to random forest generate


   #  go_slim         =    "goslim_yeast"
   #  datasets        =    ["Combined-AP/MS"]
   #  taxon_id        =    "4932"
   #  outputfiles_name=   "test"
   #  disorder_names= ['disembl-HL',"espritz-nmr"]
   #  sgd            =   True
   #  dis            =   True

   #  generate_random_forest(go_slim, outputfiles_name, datasets, taxon_id, disorder_names, sgd, dis)

   # 


    # how to random forest generate


    # go_slim             =   "goslim_yeast"
    # datasets            =   ["CCSB-YI1"]
    # taxon_id            =   "4932"
    # outputfiles_name    =   "y2h_gsd_sc"
    # disorder_names      =   ['disembl-HL',"espritz-nmr"]
    # sgd                 =   True
    # dis                 =   True

    # generate_random_forest(go_slim, outputfiles_name, datasets, taxon_id, disorder_names, sgd, dis)

    # go_slim             =   "goslim_yeast"
    # datasets            =   ["Combined-AP/MS"]
    # taxon_id            =   "4932"
    # outputfiles_name    =   "y2h_gsd_sc"
    # disorder_names      =   ['disembl-HL',"espritz-nmr"]
    # sgd                 =   True
    # dis                 =   True

    # generate_random_forest(go_slim, outputfiles_name, datasets, taxon_id, disorder_names, sgd, dis)

    # go_slim             =   "goslim_generic"
    # datasets            =   [""]
    # taxon_id            =   "9606"
    # outputfiles_name    =   "y2h_gsd_hs"
    # disorder_names      =   ['disembl-HL',"espritz-nmr"]
    # sgd                 =   False
    # dis                 =   True

    # generate_random_forest(go_slim, outputfiles_name, datasets, taxon_id, disorder_names, sgd, dis)