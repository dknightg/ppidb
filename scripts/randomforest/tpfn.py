#!usr/bin/python -o

import re
import os
import sys
import time
from random import randint
from common.djangolib import *
from django.core.exceptions import ObjectDoesNotExist




def get_tp_results(datasets, disorder_names = ['disembl-HL',  'espritz-nmr'],  go_slim_name=None, filter_for_sgd=False, filter_for_disorder=False, imputed=False ):
    """Description

    This function uses the hc_interactions table in the database to 
    generate a set of results that can be said to approximate true 
    positives

    Arguments

    datasets            The names of the datasets whose 
                        interaction_set is to be included in the 
                        results. These names are used to filter the
                        hc_interactions table on the dataset_name 
                        field.(Type=List)

    disorder_names      A list of the method names to include in the 
                        feature list. (type=List)

    go_slim_name        The go_slim_name to be used to generate the 
                        feature names. (type=String)

    filter_for_sgd      A boolean determining whether the sgd_features 
                        table should be used to generate the feature
                        names. (type=Boolean)

    filter_for_disorder A boolean determining whether the 
                        disorder_names_ should be used to generate the 
                        feature names.(type=Boolean)

    imputed             A boolean determining whether the 
                        results should include imputed values. 
                        (Type=Boolean)

    Returns 

    samples             A list of lists containing the interactors in 
                        a given interaction. (Type=Boolean)

    """
    query = hc_interactions.objects.filter(dataset_name__in = datasets).select_related()
    samples = set()

    for hc_int in query:

        interactors = hc_int.interaction_key.interactor_m2m_key.select_related()
        
        if go_slim_name!=None:
            if filter_interactor_for_go(interactors, go_slim_name)==False:
                continue
        
        if filter_for_sgd==True:
            if filter_interactor_for_sgd(interactors, imputed)==False:
                continue

        if filter_for_disorder==True:
            if filter_interactor_for_disorder(interactors, disorder_names, imputed)==False:
                continue


        samples.add(frozenset(list(interactors)))


    samples = [list(a) for a in samples]
    return samples



def filter_interactor_for_go(interactors, go_slim_name):
    """Description

    This function checks if we have go slim ids from a given go slim
    set available for a given set of interactors. If any interactor 
    does not have any go_slim annotations False is returned. Otherwise
    the list of interactors is returned

    Arguments

    interactors     An iterable containing interactor objects from the 
                    interactor table in our database.

    go_slim_name    The go_slim_name to be used to generate the 
                    feature names. (type=String)

    Returns

    interactors     An iterable containing interactor objects from the 
                    interactor table in our database. This is set to 
                    False if go slim information is missing for any 
                    of the proteins.


    """
    for a in interactors:
        if a.go_slim_set.filter(go_slim_type_m2m_key__go_slim_reference = go_slim_name).count() == 0:
            return False

    return interactors

def filter_interactor_for_disorder(interactors, disorder_names, imputed):

    """Description

    This function checks if we have disorder information for a given 
    experimental method for a given set of interactors set. If any
    interactor does not have an entry for a given experimetnal method 
    False is returned.

    Arguments

    interactors         An iterable containing interactor objects from
                        the interactor table in our database.

    disorder_names_     A list of the experimental method names to
                        include in the -feature list. (type=List)

    imputed             A boolean determining whether the 
                        results should include imputed values. 
                        (Type=Boolean)

    Returns

    interactors     An iterable containing interactor objects from the 
                    interactor table in our database. This is set to 
                    False if disorder information is missing for any 
                    of the proteins.

    """
   
    for a in interactors:
        for disorder_method in disorder_names:
            try:
                dis_obj = a.mobi_disorder_set.filter(mobi_disorder_method=disorder_method)[0]
                if dis_obj.imputed == True:
                    return False

            except IndexError:
                return False

    return interactors

def filter_interactor_for_sgd(interactors, imputed):

    """Description

    This function checks if we have sgd information for a given 
    experimental method for a given set of interactors set. If any
    interactor does not have sgd information for a given method False
    is returned.

    Arguments

    interactors         An iterable containing interactor objects from
                        the interactor table in our database.


    imputed             A boolean determining whether the 
                        results should include imputed values. 
                        (Type=Boolean)

    Returns

    interactors     An iterable containing interactor objects from the 
                    interactor table in our database. This is set to 
                    False if disorder information is missing for any 
                    of the proteins.

    """

    if imputed ==False:
        for a in interactors:
            for b in a.sgd_features_set.all():
                c=(b.sgd_features_fop_imputed, b.sgd_features_gravy_imputed, b.sgd_features_length_imputed, b.sgd_features_weight_imputed, b.sgd_features_cai_imputed)
                if True in c:
                    return False
            if a.sgd_features_set.all().count()==0:
                return False
    return interactors


def get_n_tn_results(n, taxon_ids, disorder_names=['disembl-HL',  'espritz-nmr'], go_slim_name=None, filter_for_sgd=False, filter_for_disorder=False, imputed = False ):

    """Description

    This function uses generates spurious pseudo random 
    interaction_set from the interactors stored in the database. 
    It does not include those interactions that are already known.

    Arguments

    n                   The number of results to return.(Type=Integer)

    datasets            The names of the datasets whose 
                        interaction_set is to be included in the 
                        results. These names are used to filter the
                        hc_interactions table on the dataset_name 
                        field.(Type=List)

    disorder_names      A list of the method names to include in the 
                        feature list. (type=List)

    go_slim_name        The go_slim_name to be used to generate the 
                        feature names. (type=String)

    filter_for_sgd      A boolean determining whether the sgd_features 
                        table should be used to generate the feature
                        names. (type=Boolean)

    filter_for_disorder A boolean determining whether the 
                        disorder_names_ should be used to generate the 
                        feature names.(type=Boolean)

    imputed             A boolean determining whether the 
                        results should include imputed values. 
                        (Type=Boolean)

    Returns 

    samples             A list of lists containing the interactors in 
                        a given interaction. (Type=Boolean)

    """

    if type(taxon_ids)==str:
        taxon_ids = [taxon_ids]

    resultSet       = set()
    pair_set        = set()
    results_dict    = {}

    protein_list    = interactor.objects.filter(species_key__taxon_id__in = taxon_ids).select_related()

    len_protein_list = protein_list.count()


    samples  = []
    pair_set = set()

    while len(samples) < n:
        #print len_protein_list

        a_ind = randint(0, len_protein_list-1)
        b_ind = randint(0, len_protein_list-1)

        pair  = frozenset([a_ind, b_ind])

        if pair in pair_set:
            continue
        else:
            pair_set.add(pair)

        a = protein_list[a_ind]
        b = protein_list[b_ind]

        
        if go_slim_name!=None:
            if filter_interactor_for_go([a,b], go_slim_name)==False:
                continue
        
        if filter_for_sgd==True:
            if filter_interactor_for_sgd([a,b], imputed)==False:
                continue

        if filter_for_disorder==True:
            if filter_interactor_for_disorder([a,b], disorder_names, imputed)==False:

                continue


        if a.interaction_set.count() > 0:
            a_interactions      = a.interaction_set.all()
            a_interactions_set  = set(a_interactions)

        if b.interaction_set.count() > 0:
            b_interactions      = b.interaction_set.all()
            b_interactions_set  = set(b_interactions)
        
        if len(a_interactions_set.intersection(b_interactions_set)) == 0:
                samples.append([a,b])
        
    return samples