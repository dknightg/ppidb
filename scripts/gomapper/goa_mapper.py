import os
import sys
import subprocess

from django.core.exceptions import ObjectDoesNotExist

from common.djangolib import *
import common.idmapper as idmapper

"""

This module contains a variety of functions that are involved in 
writing the contents of a goa file to the database.

"""




def goa_line_info(line):

    """Description      

    This function parses strings that are formatted as per a goa file 
    and returns the protein id and the go id from the string.

    Arguments 

    line            A string in the format of a line from a goa file
                    (Type=String)
    
    Returns

    protein_id      The protein id as given in the line (Type=String)

    go_id_line      The go id as given in the line (Type=String)

    """


    goa_line=line.split("\t")

    protein_id      = goa_line[1]
    go_id_line      = goa_line[4]
    ev_code_line    = goa_line[6]

    return protein_id, go_id_line



def new_interactee(new_id,old_id,interactees, databasename):

    """Description

    This function checks to see if the protein id for the goa file has
    changed and if it has returns a new queryset of interactees 
    otherwise it returns the interactees queryset passed to it.

    Whether there should only be one entry in biogrid per synonym is 
    up for debate. However it seems there is one case where theis is
    not the case (for Uniprot IDs no less). Whether this is a defect
    in biogrid is unclear. As a result the queryset is used rather 
    than a ind vidual interactor object.

    Arguments 

    new_id          The protein id of the current line in the goa file
                    (Type=String)

    old_id          The protein id of the previous line in the goa 
                    file.(Type=String)

    interactees     The current queryset of interactees 
                    (Type=django.db.models.query.QuerySet)


    databasename    The databasename to match when looking in the 
                    interactor_synonyms table. The database names as 
                    known to biogrid are written here along with their
                    common names are written here:

                    http://wiki.thebiogrid.org/doku.php/identifiers. 

                    This should be the "biogrid name" of the 
                    databasename that the ids in the goa file refer 
                    to. (Type=string)

    Returns

    interactees     The current queryset of interactees 
                    (Type=django.db.models.query.QuerySet)


    """

    if new_id!=old_id:
        
        interactees = interactor_synonyms.objects.filter(synonym_db = databasename, synonym = new_id)      
        interactees = [i.interactor_key for i in interactees]
        if interactees == []:
            print "\tNo match in DB for protein_id {}, {}".format(databasename, new_id    )
            return None

    return interactees



def import_goa_file_to_go(goa_file_obj, databasename):

    """Description 

    This function writes the contents of the goa_file_obj into the go 
    table in our database, setting up the necesary links to proteins
    as it does so.

    Arguments 

    goa_file_obj    The goa formatted file. (Type=file)

    databasename    The databasename to match when looking in the 
                    interactor_synonyms table. The database names as 
                    known to biogrid are written here along with their
                    common names are written here:

                    http://wiki.thebiogrid.org/doku.php/identifiers. 

                    This should be the "biogrid name" of the 
                    databasename that the ids in the goa file refer 
                    to. (Type=string)

    Returns

    None
    """

    old_id = None
    interactees = None

    for line in goa_file_obj:
        
        if line.startswith("!")!= True:
            


            protein_id, go_id_line = goa_line_info(line)
            

            interactees = new_interactee(protein_id,old_id,interactees, databasename)
            old_id=protein_id


            if interactees == None:
                continue

            print "\tWriting {}, {}".format(protein_id, go_id_line)


            go_db_obj = go.objects.get_or_create(go_id = go_id_line)[0]
            for a in interactees:
                go_db_obj.interactor_m2m_key.add(a)

    return



def import_goa_file_to_go_slim(goa_str, databasename, go_slim_name):

    """Description 

    This function writes the contents of the goa_str into the go_slim 
    table in the database, setting up the necesary links to proteins 
    as it does so. As well as linking the go_slim to the 
    go_slim_reference table detailing the name of the go_slim used to 
    generate the go_slim.

    Arguments 

    goa_str         A goa formatted string complete with newlines.
                    (Type=String)

    go_slim_name    The name of the go slim that will be referred to 
                    in the database. By convention the same as the 
                    the go_slim_name to be used to generate the 
                    feature names. (Type=String)

    databasename    The databasename to match when looking in the 
                    interactor_synonyms table. The database names as 
                    known to biogrid are written here along with their
                    common names are written here:

                    http://wiki.thebiogrid.org/doku.php/identifiers. 

                    This should be the "biogrid name" of the 
                    databasename that the ids in the goa file refer 
                    to. (Type=string)

    Returns

    None



    """

        
    print "\tReading input file: %s" % goa_fn

    old_id = None
    interactee = None

    interactees = None

    go_slim_type_db_obj = go_slim_type.objects.get_or_create(go_slim_reference = go_slim_name)
    #import pdb; pdb.set_trace()

    for line in goa_str.split("\n"):
        #print line
        if (line.startswith("!")!= True) and (line!= ""):
            


            protein_id, go_id_line = goa_line_info(line)
            

            interactees = new_interactee(protein_id,old_id,interactees, databasename)
            old_id=protein_id

            print protein_id, old_id, go_id_line, 

            if interactees == None:
                print "\tas"
                continue

            print "\tWriting go slim {}, {}".format(protein_id, go_id_line)


            go_slim_db_obj = go_slim.objects.get_or_create(go_slim_id = go_id_line)[0]
            go_type_obj    = go_slim_type.objects.get_or_create(go_slim_reference = go_slim_name)[0]
            for a in interactees:
                go_slim_db_obj.interactor_m2m_key.add(a)
            
            go_slim_db_obj.go_slim_type_m2m_key.add(go_type_obj)

    return



def map2slim(go_slim_fn, gene_ontology_fn, goa_fn):

    """Description 

    This function runs the map2slim program from the go-perl package 
    in order to map a goa file onto the goa file. Python picks up the 
    output on STDOUT and stores it in memory. As a result this 
    function may run out of ram for particuarly large goa files. 
    It returns the output from STDOUT which is formatted as per a goa
    file.

    Arguments 

    go_slim_fn          The filepath of the go slim file in the 
                        "gomapper" folder in the data directory.
                        (Type=String) 

    gene_ontology_fn   The filepath of the gene ontlology file
                        (Type=String) 

    goa_fn              The filepath of the goa file containing 
                        the annotations to be mapped onto a go_slim
                        (Type=String) 

    Returns

    data                The goa formatted output of the map2slim 
                        command. (Type=String)


    """

    map2slim     = subprocess.Popen(["map2slim", go_slim_fn, gene_ontology_fn, goa_fn],stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    data, errors = map2slim.communicate()

    return data




def process_goa_file(goa_fn, go_slim_fn, gene_ontology_fn, databasename, go_slim_name):

    """Description

    This function takes a goa file and translates it into the relevant
    go slim and populates the database tables go_slim, go and
    go_slim_reference, as well as linking those tables to the proteins 
    stored in the goa file.
    
    Arguments

    go_slim_fn          The filepath of the go slim file in the 
                        "gomapper" folder in the data directory.
                        (Type=String) 

    gene_ontology_fn   The filepath of the gene ontlology file
                        (Type=String) 

    goa_fn              The filepath of the goa file containing 
                        the annotations to be mapped onto a go_slim
                        (Type=String) 

    databasename    The databasename to match when looking in the 
                    interactor_synonyms table. The database names as 
                    known to biogrid are written here along with their
                    common names are written here:

                    http://wiki.thebiogrid.org/doku.php/identifiers. 

                    This should be the "biogrid name" of the 
                    databasename that the ids in the goa file refer 
                    to. (Type=string)

    go_slim_name:   The name of the go slim that will be referred to 
                    in the database. By convention the same as the 
                    the go_slim_name to be used to generate the 
                    feature names. (Type=String)

    Returns 

    None


    """


    print "\tMapping to slim"
    go_slim_goa_file_obj = map2slim(go_slim_fn, gene_ontology_fn, goa_fn)

    print "\tOpening goa_fn to insert into database"
    goa_file_obj = open(goa_fn, "r")
    
    import_goa_file_to_go(goa_file_obj, databasename)



    import_goa_file_to_go_slim(go_slim_goa_file_obj, databasename, go_slim_name)

    return


if __name__ == '__main__':

    """
    This allows this module to be run as a script with the following syntax:

    goa_go_mapper.py [goa file] [databaserefname] [go_slim_fn]

    """
    try:
        goa_fn          = os.path.abspath(sys.argv[1])
        databasename    = sys.argv[2] # database name is required for translation to our database ids
        go_slim_fn      = os.path.abspath(sys.argv[3])      

    except IndexError:
        print "\tUsage: goa_go_mapper.py [goa file] [databaserefname] [go_slim_fn]"

    go_slim_name = os.path.splitext(os.path.basename(go_slim_fn))[0]

    gene_ontology = os.path.abspath(os.path.join(data_dir,"gomapper/gene_ontology_ext.obo"))

    process_goa_file(goa_fn, go_slim_fn, gene_ontology, databasename, go_slim_name)