from common.djangolib import *
from django.core.exceptions import ObjectDoesNotExist
from randomforest.featuresandclasses import feature_names_gen
import numpy as np
import subprocess
import string
from random import choice
from itertools import izip
from fractions import Fraction




def gen_classes(feature_names):

    """Description

    This function generates a list of classes that can be understood 
    by the R programming language for a given list of feature_names.

    Arguments

    feature_names   A list of the feature_names that are being 
                    predicted. (Type=List)

    Returns

    classes         A list of R classes that correspond to the 
                    features of the proteins in feature_names and 
                    features. (Type=List)

    """

    classes=[]
    for name in feature_names:

        if name.startswith("GO"):
            classes.append("factor")
        
        else:
            classes.append("numeric")

    return classes


def get_features_na(interactor_queryset, feature_names, disorder_names, go_slim_name,  **kwargs):

    """Description 

      This function returns a masked array containing the values 
    for the feature_names for the interactors contained in 
    interactor_queryset.

    Arguments 

    interactor_queryset     An iterable of containing interactor 
                            objects from the database.
                            (Type=django.db.models.query.QuerySet)

    feature_names           A list of feature_names. (type=List)

    disorder_names          A list of the method names to include 
                            in the feature list. (type=List)

    go_slim_name            The name of the go slim file in the 
                            "gomapper" folder in the go_id directory 
                            of that name with a .obo file extension.
                            (type=String)

    **kwargs                A list of further keyword arguments that
                            contain the arguments:
                        

    filter_for_sgd      A boolean determining whether the sgd_features
                        table should be used to generate the feature 
                        names.(type=Boolean)

    filter_for_disorder A boolean determining whether the 
                        disorder_names_ should be used to generate 
                        the feature names. (type=Boolean).

    Returns

    features            A numpy array containing the values of the 
                        interacting proteins features. The mask on the 
                        array masks the values that are either absent 
                        from the database or have been imputed 
                        previously. (Type=numpy.ma.core.MaskedArray)

    """

    #############################    
    #####HELPER FUNCTIONS########
    #############################
    
    def sgds_feature_pop(features, feature_names, protein, i):
        """

        Arguments:

        features:       A masked numpy array

        feature_names:  A list of the feature_names that are being predicted 
                        (used for indexing)

        protein:        An interactor object from the database fro which the 
                        features table is being populated

        i:              The index of the row in features that is to be populated

        Description:    

        This is a helper function that populates the features array with the features 
        stored in the sgd_features array for a given protein. 

        """

        sgds = protein.sgd_features_set.values()


        sgd_feature_names = ['sgd_features_gravy',
                             'sgd_features_weight',
                             'sgd_features_length',
                             'sgd_features_cai',
                             'sgd_features_fop']

        if sgds.count()!=0:

            sgd=sgds[0]

            for sgd_feature in sgd_feature_names:

                j = feature_names.index(sgd_feature)

                if sgd[sgd_feature] == None or sgd[sgd_feature+"_imputed"]==True:
                    features.mask[i][j] = True
                else:
                    features.data[i][j] = sgd[sgd_feature]

        else:
            for sgd_feature in sgd_feature_names:
                j = feature_names.index(sgd_feature)
                features.mask[i][j] = True

        return features

    def go_features_pop(features, feature_names, protein, go_slim_name, i):

        """
        Arguments 

        features        A masked numpy array

        feature_names   A list of the feature_names that are being predicted 
                        (used for indexing)

        protein         An interactor object from the database fro which the 
                        features table is being populated

        go_slim_name    The name of the go slim file in the "gomapper" folder in the 
                        go_id directory of that name with a .obo file extension.

        i               The index of the row in features that is to be populated

        Description     

        This is a helper function that populates the features array with the GO ids 
        linked to in the sgd_features array for a given protein. 

        """

        go_ids    =   protein.go_slim_set.filter(go_slim_type_m2m_key__go_slim_reference = go_slim_name).values_list("go_slim_id", flat = True)

        if go_ids.count()!=0:
            for go_id in go_ids:
                j = feature_names.index(go_id)
                features.data[i][j] = 1

        else:
            for j, feature_name in enumerate(feature_names):
                if feature_name.startswith("GO"):
                    features.mask[i][j] = True

        return features


    def disorder_features_pop(features, feature_names, protein, i):

        """
        Arguments 

        features        A masked numpy array

        feature_names   A list of the feature_names that are being predicted 
                        (used for indexing)

        protein         An interactor object from the database fro which the 
                        features table is being populated

        i               The index of the row in features that is to be populated

        Description     

        This is a helper function that populates the features array with the features 
        stored in the mobi_disorder table for a given protein. 

        """


        for disorder_name in disorder_names:
            tallys       =   protein.mobi_disorder_set.filter(mobi_disorder_method = disorder_name).filter(imputed=False).values()
            j = feature_names.index(disorder_name)
            if tallys.count()!=0:

                tallys=tallys[0]
                disorder            =     float(tallys["mobi_disorder_Tally_One"])/(tallys["mobi_disorder_Tally_Zero"] + tallys["mobi_disorder_Tally_One"])              
                features.data[i][j]      =     disorder

            else:
                features.mask[i][j]      =     True


        return features




    #################################    
    #####FUNCTION STARTS HERE########
    #################################

    n = interactor_queryset.count()
    
    a           =   np.zeros((n,len(feature_names)))
    features    =   np.ma.array(a, mask=a, fill_value = np.nan)


    for i, protein in  enumerate(interactor_queryset):

 

        features    =   go_features_pop(features, feature_names, protein, go_slim_name, i)

        if kwargs["filter_for_sgd"] == True:
            features    =   sgds_feature_pop(features, feature_names, protein, i)

        if kwargs["filter_for_disorder"] == True:
            features    =   disorder_features_pop(features, feature_names, protein, i)

    return features


def gen_na_features(taxid, **kwargs):
    """Description

    This function imputes the missing features in the tables 
    "sgd_features" and mobi_disorder for the proteins of a given 
    species.

    Arguments 

    taxid               A string of the taxonomical id of the proteins
                        for whom the interaction is predicted.
                        (Type=List)

    **kwargs            A list of keyword arguments that contains the 
                        following keyword arguments


    filter_for_sgd      A boolean determining whether the sgd_features 
                        table should be used to generate the feature
                        names. (type=Boolean)

    filter_for_disorder A boolean determining whether the 
                        disorder_names_ should be used to generate the 
                        feature names.(type=Boolean)


    Returns

    features_imputed    A numpy array containing both the features 
                        contained within the database and the missing
                        ones for each protein. Proteins are stored 
                        one per row.
                        (Type=numpy.array)


    features_mask       A boolean array in which a True indicates that 
                        the corresponding entry in the 
                        features_imputed array was imputed
                        (Type=numpy.array)


    feature_names       A list of feature_names. (type=List)

    """
    interactor_queryset     =   interactor.objects.filter(species_key__taxon_id = taxid)

    disorder_names_ = ['disembl-465', 'disembl-HL', 'espritz-disprot', 'espritz-xray', 'espritz-nmr', 'iupred-long', 'iupred-short']

    feature_names           =   feature_names_gen(interaction = False, disorder_names_= disorder_names_, **kwargs)
    features                =   get_features_na(interactor_queryset, feature_names, disorder_names_, **kwargs)
    print features
    classes                 =   gen_classes(feature_names)
    print len(feature_names)


    print len(classes)
    features_mask   = features.mask
    masked_features = features.filled()
    features = np.vstack((np.array(feature_names), masked_features[0:]))

    features_imputed = impute_with_r(features, feature_names, classes)    

    pop_database_with_imputed_values(interactor_queryset, features_imputed, features_mask, feature_names, disorder_names_, **kwargs)

    return features_imputed, features_mask, feature_names


def impute_with_r(features, feature_names, classes):

    """Description 

    This function takes its arguments and writes them to randomly 
    generated temporary files in /tmp and then triggers the 
    impute.r script in the Rscript_dir. It returns a numpy array 
    containing both the features contained within the database 
    and the imputed ones for each protein. Proteins are stored 
    one per row.


    Arguments:

    features:           An array containing the features of the 
                        proteins. Values to be imputed are represented
                        by NaNs. (Type=numpy.array)


    feature_names       A list of feature_names. (type=List)

    classes:            A list of R classes that correspond to the 
                        features of the proteins in feature_names and 
                        features. (Type=List)

    Returns

    features_imputed    A numpy array containing both the features 
                        contained within the database and the missing
                        ones for each protein. Proteins are stored 
                        one per row.
                        (Type=numpy.array)
    
    """

    random_fn           =   ''.join(choice(string.letters + string.digits) for i in xrange(10))
    r_inputfile         =   os.path.join("/tmp", random_fn)
    np.savetxt(r_inputfile,  features,fmt="%s", delimiter=",")


    random_fn           =   ''.join(choice(string.letters + string.digits) for i in xrange(10))
    r_classfile         =   os.path.join("/tmp", random_fn)
    np.savetxt(r_classfile,  classes,fmt="%s", delimiter=",")


    random_fn           =   ''.join(choice(string.letters + string.digits) for i in xrange(10))
    r_outputfile        =   os.path.join("/tmp", random_fn)
    open(r_outputfile, 'w').close() # touch file so error in r script doesn't manifest in error in this script

    print r_inputfile, r_outputfile

    try:
        subprocess.check_call(["sed", "-i", "s/nan/NA/g", r_inputfile])
        subprocess.check_call(["Rscript", os.path.join(Rscript_dir, "impute.R"), r_inputfile, r_classfile, r_outputfile ])
        subprocess.check_call(["sed", "-i", 's/"//g', r_outputfile]) #Hack to convert from R factors which are represented as string...
        features_imputed = np.loadtxt(open(r_outputfile,"rb"), delimiter=',')#...so we can import into float matrix here


    except Exception:
        raise 

    finally:
        os.remove(r_inputfile)
        os.remove(r_classfile)
        os.remove(r_outputfile)
        
    return features_imputed


def pop_database_with_imputed_values(interactor_queryset, features_imputed, features_mask, feature_names, disorder_names, **kwargs):

    """Description 

    This function populates the database with missing features in the 
    tables "sgd_features" and mobi_disorder for the proteins of a 
    given species once the values have been imputed.

    
    Arguments 

    interactor_queryset     An iterable of containing interactor 
                            objects from the database.
                            (Type=django.db.models.query.QuerySet)



    features_imputed        A numpy array containing both the features 
                            contained within the database and the 
                            missing ones for each protein. Proteins 
                            are stored one per row.(Type=numpy.array)

    features_mask       A boolean array in which a True indicates that 
                        the corresponding entry in the 
                        features_imputed array was imputed
                        (Type=numpy.array)

    feature_names       A list of feature_names. (type=List)
    
    disorder_names_     A list of the method names to include in the 
                        feature list. (type=List)


    **kwargs            A list of further keyword arguments that 
                        contains the arguments:
                        

    filter_for_sgd      A boolean determining whether the sgd_features
                        table should be used to generate the feature 
                        names.(type=Boolean)

    filter_for_disorder A boolean determining whether the 
                        disorder_names_ should be used to generate 
                        the feature names. (type=Boolean)
    Returns

    None


    """


    for i,j in izip(*np.where(features_mask == True)):
        name            =     feature_names[j]
        protein         =     interactor_queryset[i]
        feature_value   =     features_imputed[i][j]

        print name, protein.id, feature_value, i,j

        if name.startswith("sgd") and kwargs["filter_for_sgd"]==True:
            sgd_obj                          =  sgd_features.objects.get_or_create(   interactor_key=protein)[0]
            setattr(sgd_obj, name, feature_value)
            setattr(sgd_obj,name+"_imputed", True)
            sgd_obj.save()

        if name in disorder_names and kwargs["filter_for_disorder"] ==  True:
            
            ##This is a horrible hack, to get the tallys consistent
            asfrac = Fraction(feature_value)
            t1     = asfrac.numerator
            tot    = asfrac.denominator
            t0 = tot-t1

            dis_objs = mobi_disorder.objects.filter( interactor_key  =   protein).filter(mobi_disorder_method = name)


            if dis_objs.count() ==0:
                m=mobi_disorder(  interactor_key              =   protein,
                                imputed                     =   True,
                                mobi_disorder_method        =   name,
                                mobi_disorder_Tally_Zero    =   t0,
                                mobi_disorder_Tally_One     =   t1,
                                )
                m.save()

            if dis_objs.count()==1 and dis_objs[0].imputed==True:

                dis_obj=dis_objs[0]
                dis_obj.imputed=True                 
                dis_obj.mobi_disorder_method = name
                dis_obj.mobi_disorder_Tally_Zero = t0
                dis_obj.mobi_disorder_Tally_One = t1
                dis_obj.save()

    return

if __name__ == '__main__':
	gen_na_features("4932",go_slim_name = "goslim_yeast", filter_for_sgd=True, filter_for_disorder=True)
    gen_na_features("9606",go_slim_name = "goslim_generic", filter_for_sgd=False, filter_for_disorder=True)
    


