
from common.djangolib import *

import common.idmapper as idmapper

import os
import sys

def read_ccsby( datapath_ymapper , hc_source, hc_dataset_name, taxon_id ):

    """Description 

    This function populates the hc_interaction table with the 
    interactions described in the file at the filepath given by 
    datapath_ymapper. It acts on files formatted as per the 
    CCSB-YI1 dataset.

    Arguments 

    datapath_ymapper    This is the path to the yeast file containing 
                        the high confidence interactions. This should 
                        be a tab delimited file containing the 
                        systematic names of the proteins involved 
                        in the interactions.(Type=String)

    hc_source           This is the source (in a literary sense) of 
                        the high confidence interactions by convention
                        this is a pubmed id. This will be inserted 
                        into the hc_interaction table. (Type=String)


    hc_dataset_name     This is the name of the dataset as it is 
                        referred to at the source and elsewhere in 
                        the literature. This will be inserted into the
                        hc_interaction table.(Type=String)

    taxon_id            This is the taxonomical id of the species the 
                        file pointed to by "datapath_ymapper".
                        (Type=String)

    Returns 

    None

    """

    try:
        ccsbyfn = os.path.normpath(sys.argv[1])
    except IndexError:

        # Uses data_dir to get around box-specific paths
        # datapath_ymapper = os.path.join( data_dir, "ymapper/" )
        # datapath_ymapper = os.path.join( datapath_ymapper, filename )

        ccsbyfn = datapath_ymapper

    print "Reading input file: %s" % ccsbyfn

    bg_id_dict = idmapper.bgid_to_dbid_dict( "SYSTEMATIC_NAME", bg_id_2_db_id = False )
    
    print "Opening: %s " % ccsbyfn

    ccsby_file = open(ccsbyfn, "r")

    for line in ccsby_file:
            #print line
            

            ccsby_line=line.split("\t")

            try:
                
                protid_a_from_line      = ccsby_line[0].strip()
                protid_a_from_line      = bg_id_dict[protid_a_from_line]

                protid_b_from_line      = ccsby_line[1].strip()
                protid_b_from_line      = bg_id_dict[protid_b_from_line]
                

            except KeyError:
                print "Dictionary lookup failed on line: %s" % line
                
                continue


            
            write_ccsby_to_ppidb(protid_a_from_line, protid_b_from_line, hc_source, hc_dataset_name, taxon_id)

    return

def write_ccsby_to_ppidb(protid_a, protid_b, hc_source, hc_dataset_name, taxon_id):
    
    """Description:

    This function populates the hc_interaction table with one entry 
    per interaction present in the interaction table that matches the
    given protein ids.

    

    write_ccsby_to_ppidb(protid_a, protid_b, hc_source, hc_dataset_name, taxon_id)

    Arguments 

    protid_a            The biogrid id of the first protein in the 
                        interaction. (Type=String)

    protid_b            The biogrid id of the second protein in the 
                        interaction. (Type=String)


    hc_source           This is the source (in a literary sense) of 
                        the high confidence interactions by convention
                        this is a pubmed id. This will be inserted 
                        into the hc_interaction table. (Type=String)


    hc_dataset_name     This is the name of the dataset as it is 
                        referred to at the source and elsewhere in 
                        the literature. This will be inserted into the
                        hc_interaction table.(Type=String)

    taxon_id            This is the taxonomical id of the species the 
                        file pointed to by "datapath_ymapper".
                        (Type=String)

    Returns

    None
    """

    print "Writing: ",protid_a, protid_b
    interactions = idmapper.get_exact_match(interaction, "interactor_m2m_key__biogrid_id",[protid_a, protid_b])
    species_obj  = species.objects.get(taxon_id = taxon_id)
    
    for interactee in interactions:

        hc_interactions.objects.get_or_create(  source          =   hc_source,
                                                dataset_name    =   hc_dataset_name,
                                                interaction_key =   interactee,
                                                species_key     =   species_obj)


    return




if __name__ == '__main__':
    read_ccsby("../../data/ymapper/CCSB-Y2H.txt", "PMID: 18719252", "CCSB-YI1", "4932")
    #read_ccsby("../../data/ymapper/combinedapms.txt", "PMID: 17200106", "Combined-AP/MS", "4932")

