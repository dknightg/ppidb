import os
import sys
import time 

from common.djangolib import *

### Our modules:
#------------------------------------------------


import common.idmapper              as idmapper

import parsebio.parsebio            as bio
import sgd_annotater.sgd_annotater  as sgd
import disorder.MobiDBparse         as mobi
import gomapper.goa_mapper          as gmap 
import common.synonym_mapper        as smap

import yeastmapper.ymapper          as hc_y
import hsmapper.hsmapper            as hc_h
#------------------------------------------------



t0_metaparser  = time.clock()  # for measuring the DB building duration
""" 
Running this script essentially populates an empty database into one 
with all the necessary information from source data files. 

It sequentially executes all functions from separate scripts, 
coordinating them so that all functions with dependencies to other 
functions implemented later, and those with no dependencies implement
-ed first. 

"""



#---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#       
#                              meta_parse                                  |
#                              |_____________________                      |
#                              |                     \                     |
#                              parsebio               synonym              |
#                              |                       |                   |
#                              |                       |                   |
#                              idmapper                goa                 |
#                        ______|______                 |_______            |
#                      /       |       \               |       \           |
#                     hc       sgd     mobi            human    yeast      |
#               ______|__      |       |______         |        |          |
#              /         |     |       |      \        |        |          |
#             yMap    hcHitP   yeast   human   yeast   |        |          |
#         ____|         |      |       |       |       |        |          |
#        /    |       human    |_______|_______|_______|________|          |
#       yeast human            |               |                           |
#                              impute          rf_generator                |        <-- you are here
#                              |               |__                         |
#                              |_______________|  \                        |
#                              |                   rf_acc                  |
#                              rf_pval                                     |
#       
#                      
#       
#
#---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

# PARSEBIO
#-----------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------
print '\t','Parsing: biogrid --> ppidb...'
datapath_bio        = os.path.join( data_dir, 'parsebio/biogrid.mitab.txt')

bio.biogrid_to_django(  datapath_bio,\
                        startingLine = 1 )
# Check:
#-----------------------------------------------------------------
count_entries = interaction.objects.all().count()
if count_entries == 0:          # if no entries added -->
    print '\tERROR:No entries in interaction table exiting...'
    sys.exit() 
else:                           # else print number of entries
    print '\tentries added: '+str(count_entries)
#-----------------------------------------------------------------



# SGD FEATURES
#-----------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------
print '\t','Parsing: sgd --> ppidb...'
datapath_sgd        = os.path.join( data_dir, 'sgd_annotater/sgd_protein_features.tab' )
datapath_idmap_sgd  = os.path.join( data_dir, 'common/biogrid_identifiers.txt' )

sgd.sgd_to_django(  datapath_sgd,\
                    datapath_idmap_sgd,\
                    startingLine = 1 )
# Check:
#-----------------------------------------------------------------
count_entries = sgd_features.objects.all().count()
if count_entries == 0:          # if no entries added -->
    print '\tERROR:No entries in sgd_features table exiting...'
    sys.exit() 
else:                           # else print number of entries
    print '\tentries added from sgd: '+str(count_entries)
#-----------------------------------------------------------------

# MOBI - HUMAN
#-----------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------
print '\t','Parsing: mobi: human data --> ppidb...'
datapath_mobi       = os.path.join( data_dir, 'disorder/annotations_homosapiens.fasta' )
#                                                               ^ for human
datapath_idmap_mobi = os.path.join( data_dir, 'common/biogrid_identifiers.txt' )

mobi.mobi_to_django(    datapath_mobi,\
                        datapath_idmap_mobi,\
                        startingLine = 1 )
#                           ^ variable to each box: fetched by djanglolib <-- config.cfg }} hard-edit these in config.cfg per box
#                                      ^ these are set to defaults within each script and hard edited in each script
# Check:
#-----------------------------------------------------------------
count_entries = mobi_disorder.objects.filter( interactor_key__species_key__taxon_id = '9606' ).count()
#                                                                                        ^ HUMAN taxon id
#                                       ^extract      ^-->via key  ^-->model ^-->this field within model
#                                                                                                   ^ count
if count_entries == 0:          # if no entries added -->
    print '\tERROR: No entries in mobi_disorder table exiting... '
    sys.exit() 
else:                           # else print number of entries 
    print '\tEntries added'+str(count_entries)
#-----------------------------------------------------------------


# MOBI - YEAST
#-----------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------
print '\t','Parsing: mobi: yeast data --> ppidb...'

# Args
datapath_mobi       = os.path.join( data_dir, 'disorder/annotations_sc.fasta' )
            #                                           ^ for yeast
datapath_idmap_mobi = os.path.join( data_dir, 'common/biogrid_identifiers.txt' )

# Run
mobi.mobi_to_django(    datapath_mobi,\
                        datapath_idmap_mobi,\
                        startingLine = 1 )
#                           ^ variable to each box: fetched by djanglolib <-- config.cfg }} hard-edit these in config.cfg per box
#                                      ^ these are set to defaults within each script and hard edited in each script
# Check:
#-----------------------------------------------------------------
count_entries = mobi_disorder.objects.filter( interactor_key__species_key__taxon_id = '4932' ).count()
if count_entries == 0:          # if no entries added -->
    print '\tNo entries'
    sys.exit() 
else:                           # else print number of entries 
    print '\tEntries added'+str(count_entries)
#-----------------------------------------------------------------



# SYNONYM MAPPER:            <--- allows translation between protein names --> used by GOA mapper
#-----------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------
print '\t Generating a synonym_map...'

# ARG
datapath_synonymMapper = os.path.join( data_dir, "common/biogrid_identifiers.txt" )
# RUN
smap.synonym_mapper(datapath_synonymMapper) 

# Check:
#-----------------------------------------------------------------
count_entries = interactor_synonyms.objects.all().count()
if count_entries == 0:          # if no entries added -->
    print '\tNo entries'
    sys.exit() 
else:                           # else print number of entries 
    print '\tEntries added'+str(count_entries)
#-----------------------------------------------------------------



# GOA MAPPER - HUMAN
#-----------------------------------------------------------------------------------------------------

gene_ontology   = os.path.abspath(  os.path.join( data_dir, 'gomapper/gene_ontology_ext.obo'))

#-----------------------------------------------------------------------------------------------------
print '\t Parsing: go_slim: human data --> ppidb...'

# Args
goa_fn          = os.path.join(     data_dir, 'gomapper/gene_association.goa_human')
databasename    = 'SWISSPROT'  # db name for translation to our db ids   --> yeast / human
go_slim_fn      = os.path.join(     data_dir, 'gomapper/goslim_generic.obo')      # go slim file name                       --> yeast / human
go_slim_name    = os.path.splitext( os.path.basename(  go_slim_fn ))[0]
 
# Run
gmap.process_goa_file(      goa_fn, go_slim_fn,\
                            gene_ontology,\
                            databasename,\
                            go_slim_name    )
# Check go:
#-----------------------------------------------------------------
count_entries = go.objects.filter( interactor_m2m_key__species_key__taxon_id = '9606' ).count()
if count_entries == 0:          # if no entries added -->
    print '\tNo entries'
    sys.exit() 
else:                           # else print number of entries 
    print '\tEntries added'+str(count_entries)
#-----------------------------------------------------------------
# Check go_slim:
#-----------------------------------------------------------------
count_entries = go_slim.objects.filter( go_id_m2m_key__interactor_m2m_key__species_key__taxon_id = '9606' ).count()
if count_entries == 0:          # if no entries added -->
    print '\tNo entries'
    sys.exit() 
else:                           # else print number of entries 
    print '\tEntries added'+str(count_entries)
#-----------------------------------------------------------------



# GOA MAPPER - YEAST
#-----------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------
print '\t Parsing: go_slim: yeast data --> ppidb...'

# Args
goa_fn          = os.path.join(     data_dir, 'gomapper/gene_association.goa_yeast')
databasename    = 'SGD'   # db name for translation to our db ids   --> yeast / human
go_slim_fn      = os.path.join(     data_dir, 'gomapper/goslim_yeast.obo')      # go slim file name                       --> yeast / human
go_slim_name    = os.path.splitext( os.path.basename( go_slim_fn ))[0]

# Run 
gmap.process_goa_file(      goa_fn, go_slim_fn,\
                            gene_ontology,\
                            databasename,\
                            go_slim_name    )
# Check go:
#-----------------------------------------------------------------
count_entries = go.objects.filter( interactor_m2m_key__species_key__taxon_id = '4932' ).count()
if count_entries == 0:          # if no entries added -->
    print '\tNo entries'
    sys.exit() 
else:                           # else print number of entries 
    print '\tEntries added'+str(count_entries)
#-----------------------------------------------------------------
# Check go_slim:
#-----------------------------------------------------------------
count_entries = go_slim.objects.filter( go_id_m2m_key__interactor_m2m_key__species_key__taxon_id = '4932' ).count()
if count_entries == 0:          # if no entries added -->
    print '\tNo entries'
    sys.exit() 
else:                           # else print number of entries 
    print '\tEntries added'+str(count_entries)
#-----------------------------------------------------------------




# HIGH CONFIDENCE - YEAST - ccsby2h
#-----------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------
datapath_ymapper = os.path.join( data_dir,"ymapper/CCSB-Y2H.txt" )
hc_y.read_ccsby( datapath_ymapper, "PMID: 18719252", "CCSB-YI1", "4932" )         # dataset 1 
# Check go:
#-----------------------------------------------------------------
count_entries = hc_interactions.objects.filter( species_key__taxon_id = '4932' ).filter( dataset_name='CCSB-YI1' ).count()
if count_entries == 0:          # if no entries added -->
    print '\tNo entries'
    sys.exit() 
else:                           # else print number of entries 
    print '\tEntries added'+str(count_entries)
#--------------------------------------------------------------f---




# HIGH CONFIDENCE - YEAST - capms
#-----------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------
datapath_ymapper = os.path.join( data_dir, "ymapper/combinedapms.txt" )
hc_y.read_ccsby( datapath_ymapper, "PMID: 17200106", "Combined-AP/MS", "4932" )   # dataset 2
# Check:
#-----------------------------------------------------------------
count_entries = hc_interactions.objects.filter( species_key__taxon_id = '4932' ).filter( dataset_name='Combined-AP/MS' ).count()
if count_entries_ == 0:          # if no entries added -->
    print '\tNo entries for go_slim'
    sys.exit() 
else:                           # else print number of entries
    print '\tentries added from go_slim: '+str(count_entries)
#-----------------------------------------------------------------



# HIGH CONFIDENCE - HUMAN - H_sapiens_ss
#-----------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------
datapath_hsmapper            = os.path.join(data_dir,'hsmapper/HI2_2011.tsv')

hc_h.read_ccsbhs(datapath_hsmapper, "PMID: 21516116", "HI2_2011", "9606")
# Check:
#-----------------------------------------------------------------
count_entries = hc_interactions.objects.filter( species_key__taxon_id = '9606' ).filter( dataset_name='H_sapiens_ss' ).count()
if count_entries_ == 0:          # if no entries added -->
    print '\tNo entries for go_slim'
    sys.exit() 
else:                           # else print number of entries
    print '\tentries added from go_slim: '+str(count_entries)
#-----------------------------------------------------------------





#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

t_metaparser = time.clock()

t_metaparser = t_metaparser - t0_metaparser


print t_metaparser
