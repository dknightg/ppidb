import sys, os

import ConfigParser

"""
  This module is in general responsible for setting various parameters that are accessed throughout the scripts
present in the scripts directory.

It also appends the django directory to the python path so the scripts can access the databas eusing the interfaces 
provided by django

"""

config      	= ConfigParser.RawConfigParser()

# PPIdb or PPIdb2 							
config.read(os.getenv('PPIDBCONFIG'))

# PPIdb or PPIdb2
database    	= config.get('database', 'database')

# data directory 
data_dir	 	= config.get('localdirs', 'data')

# Scripts directory  our R scripts
Rscript_dir		= config.get('localdirs', 'Rscripts')

# Append django directory to python path:
sys.path.append(os.path.join(
    os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))), database))




print "Appending django dir python path: %s" % os.path.join(
    os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))), database )

# Then we can import settings and apply the django_environment settings.

import settings

from django.core.management import setup_environ

setup_environ(settings)

# Now we can import a few useful functions and all the models form PPbiogrid.

from django.db.models import Avg, Max, Min, Count
from PPIbiogrid.models import *









