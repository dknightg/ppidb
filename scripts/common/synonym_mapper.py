from common.djangolib import *

from django.core.exceptions import ObjectDoesNotExist

import os

def synonym_mapper( datapath_synonymMapper):

    """Description

    This function populates the interactor_synonyms table in the 
    database from a biogrid identifiers file.
    
    Arguments

    datapath_synonymMapper  The filepath of the go slim file in the 
                            "gomapper" folder in the data directory.
                            (Type=String) 


    Returns 

    None


    """

    bgfilename  =   os.path.abspath( datapath_synonymMapper )

    bgfile      =   open(bgfilename, "r")

    count = 0

    bgid = ""

    flag = 0
    
    interactee = None

    int_synonyms =[]

    # MAIN LOOP:
    print '\tGenerating protein synonym map...'
    for line in bgfile:


        if flag !=1:
            if line.startswith("BIOGRID_ID\tIDENTIFIER_VALUE\tIDENTIFIER_TYPE\tORGANISM_OFFICIAL_NAME"):
                    flag=1
            continue



        splitline = line.split("\t")

        line_bgid       = splitline[0]
        line_synonym    = splitline[1]
        line_synonym_db = splitline[2]

        if line_bgid!=bgid:
            
            
            bgid = line_bgid


            try:
                interactee  = interactor.objects.get(biogrid_id=line_bgid)
 

            except ObjectDoesNotExist:
                interactee=None
                

        
        if interactee==None:
            continue   
                    

        int_synonyms.append(interactor_synonyms(synonym             =   line_synonym,
                                                synonym_db          =   line_synonym_db,
                                                interactor_key      =   interactee          ))

        count = count + 1

        if count == 10000:
            interactor_synonyms.objects.batch_insert(*int_synonyms)
            
            int_synonyms=[]
            
            count = 0
    
    
    interactor_synonyms.objects.batch_insert(*int_synonyms)



    return


if __name__ == '__main__':
    
    datapath_synonymMapper = 'common/biogrid_identifiers.txt'

    synonym_mapper( os.path.join( data_dir,datapath_synonymMapper ) )

