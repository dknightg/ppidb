import pickle
import os
import sys
from Bio import Entrez
from common.djangolib import *

Entrez.email = "dtk12@ic.ac.uk"

"""

This module contains functions and classes involved in mapping between 
both taxonomical ids and identifiers for proteins.


"""

class SpeciesNotFoundError(Exception):

    """Description

    A custom error class raised by SpeciesMap that covers the case 
    where a species is not found in the Entrez database for a given
    taxon id.
    
    """

    pass

def get_exact_match(model_class, filter_field, ids):
    """Description 

    This function is intended to find those objects in a table that 
    have many to many relationships with certain objects in other 
    tables. To that end it acts on the table represented by 
    model_class and filters those objects by the relationships of the 
    objects in that table.

    The relationship to be filtered on is specified by filter_field.


    Arguments 

    model_class     An object of the 
                    (Type=django.db.models.base.ModelBase)
    
    filter field    The relationship to be mapped though from the 
                    model_class table.(Type=String)

    ids             The ids to be filtered by (Type=List)

    Returns

    query           The filtered query against model_class.
                    (Type=django.db.models.query.QuerySet)

    """
    query = model_class.objects.annotate(count=Count(filter_field)).filter(count=len(set(ids)))
    
    for _id in ids:
        query = query.filter(**{filter_field:_id})

    return query




def bgid_to_dbid_dict( database_name, datapath_idmapper = os.path.join(data_dir,"common/biogrid_identifiers.txt"), bg_id_2_db_id = True ):
    
    """Description

    This function returns a dictionary that can be used to map between
    biogrid ids and the synonyms for those ids used by another 
    database.

    Arguments

    databasename    The databasename to match when looking in the 
                    interactor_synonyms table. The database names as 
                    known to biogrid are written here along with their
                    common names are written here:

                    http://wiki.thebiogrid.org/doku.php/identifiers. 

                    This should be the "biogrid name" of the 
                    databasename that the ids in the goa file refer 
                    to. (Type=string)


    datapath_idmapper   The path to the biogrid_identifiers file as 
                        downloaded from the biogrid website. It is a
                        file that maps between biogrid identifiers and 
                        other common biogrid files. (Type=Boolean)

    bg_id_2_db_id       A boolean flag indicating whether the 
                        dictionary returned should have as its keys
                        the biogrid values (True) Or the ids of the 
                        database specified by database_name as its 
                        values (False) (Type=Boolean)

    Returns

    bg_id_dict          A dictionary translating biogrid ids to
                        synonyms or vice versa.


    """


    bgidfile = open( datapath_idmapper )
    
    bg_id_dict = {}

    startflag = 0

    for line in bgidfile:
        if  "BIOGRID_ID\tIDENTIFIER_VALUE\tIDENTIFIER_TYPE\tORGANISM_OFFICIAL_NAME" in line:
            startflag = 1

        splitline = line.split("\t")
        #print line

        if startflag ==1:
            if splitline[2]==database_name:

                bg_id = splitline[0]
                db_id = splitline[1]

                if bg_id_2_db_id == True:
                    bg_id_dict[bg_id] = db_id

                else:
                    bg_id_dict[db_id] = bg_id

    return bg_id_dict


class SpeciesMap():

    """
    Description

    This class provides an object that can be used to map taxon ids 
    of strains to the species that they refer to. It interacts with 
    the ncbis taxonomy database to do this and as this limits the
    amount of requests per second this class also implements caching
    both between requests and between calls to this function through 
    the use of pickle.

    """

    def __init__(self):
        """
        Description

        This initialises the species dictionary as well as its pickle 
        file.

        Arguments

        None


        """

        datapath_pickle = os.path.join(data_dir,'common/speciesdict.pickle')

        if os.path.exists( datapath_pickle ):
            tempfile         = open( datapath_pickle ,"r")
            try:
                self.speciesdict = pickle.load(tempfile)
            except EOFError:
                self.spfile      = open( datapath_pickle ,"w")
                self.speciesdict = {}
                print 'Encountered EOFError when opening speciesdict. Did a previous run fail to complete successfully? '
            tempfile.close()
            self.spfile      = open( datapath_pickle ,"w")
            print self.spfile.closed
        
        else:
            self.spfile      = open( datapath_pickle ,"w")
            self.speciesdict = {}

    def species_mapper(self, strain_taxon_id):
        """
        
        Description

        This function takes the taxon_id at the species or strain level
        and returns the taxon_id at the species_level. If the taxon id of 
        a species is passed to it the method returns the same taxon
        id.

        Arguments 

        strain_taxon_id        The taxon id of the strain. 
                               (Type=String)

        Returns 

        species_taxon_id       The taxon id of the species. 
                               (Type=String)

        """

        try:
            try:
                species_taxon_id = self.speciesdict[strain_taxon_id]

            except KeyError:
                handle      = Entrez.efetch( db="Taxonomy", id=strain_taxon_id, retmode="xml" )
                taxon_dict  = Entrez.read(handle)[0]
                species_taxon_id = None
                #print taxon_dict
                
                if taxon_dict["Rank"] == "species":
                    species_taxon_id = strain_taxon_id

                else:
                    #print type(taxon_dict["LineageEx"])
                    lineage = list(taxon_dict["LineageEx"])
                    #print type(lineage)
                    #print lineage
                    for entry in lineage:
                        if entry["Rank"] == "species":
                
                            species_taxon_id = entry["TaxId"]
                            break

                if species_taxon_id == None:
                    raise SpeciesNotFoundError("Species not found, please verify if id is of species level or lower")

                self.speciesdict[strain_taxon_id] = species_taxon_id
            return species_taxon_id

        except:

            print "b",self.spfile.closed
            self.closefiles()
            print "Unexpected error:", sys.exc_info()[0]
            raise

    def closefiles(self):
        """

        Description:

        This function pickles the dictionary generated, for use next 
        time. It should be called at the close of any script that uses
        SpeciesMap.

        Arguments

        None

        Returns

        None
        """


        pickle.dump(self.speciesdict,self.spfile)
        self.spfile.close()

