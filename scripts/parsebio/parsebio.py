#!usr/bin/python 

counter = 0
import re
import os
import sys
import time
import math


from common.djangolib import *

from PPIbiogrid.models import * 



import common.idmapper as idmapper

def biogrid_to_django(datapath_bio, startingLine=0):


    """
    Description

    1. Generate taxon_id map from strain level to species level.
    2. Reads the biogrid file ("biogrid.mitab.txt") line by line, 
    extracting the columns and parsing them to the database, attaching
    foreign key and m2m links according to the database shcema 
    (see "models.py" for shcema)

    Arguments

    datapath_bio    the path within which the biogrid protein 
                    interectors data is stored.
                    (Type=String)

    startingLine    The line of the datafile which the function
                    begins parsing from. This defaults to 1.
                    (Type=Integer)
    """    

    #---------------------------------
    t0              = time.clock()            
    lineCount_skip  = 0
    lineCount       = 0
    #---------------------------------

    in_file     = open( datapath_bio )       
    in_line     = in_file.readline()               
    species_map = idmapper.SpeciesMap()


    while True:
        in_line = in_file.readline()
        
        if in_line == "":
            break

        #------------------------------------------------------------------------------------------------------------------------------
        # Loading Bar::
        elapsedTime     = int(time.clock() - t0)                       
        lineCount       += 1
        percentageLeft  = int( ( (622752 - (lineCount + startingLine) )*100 )/622751. )     
        rate            = int( lineCount / (elapsedTime + 0.1) ) + 0.1                    
        #                                                      ^ prevent /0 err    
        eta             = int( (622752 - (lineCount + startingLine) ) / rate)
        # Prints every 5 secs: 
        if elapsedTime%5 < 1.:  # print ever 5 seconds to prevent slow-down
            print("Remaining %",percentageLeft,"||   lines done:",lineCount+startingLine,"||   Seconds elapsed:",elapsedTime,"||   Rate:",rate,"||   ETA:",eta) #progress bar: percentage left to complete
        #------------------------------------------------------------------------------------------------------------------------------

        splitline = in_line.split('\t')

        if len(splitline) == 15:

            # 1. Species:
            taxid1_fromBiogrid                  = re.search("(taxid\:)(\d*)", splitline[9]).group(2)          
            taxid1_fromBiogrid                  = species_map.species_mapper(taxid1_fromBiogrid)
            species1_newEntry                   = species.objects.get_or_create(taxon_id = taxid1_fromBiogrid )[0]   
            taxid2_fromBiogrid                  = re.search("(taxid\:)(\d*)", splitline[10]).group(2)          
            taxid2_fromBiogrid                  = species_map.species_mapper(taxid2_fromBiogrid)
            species2_newEntry                   = species.objects.get_or_create(taxon_id = taxid2_fromBiogrid )[0]        

            # 2. Interactor:
            interactor1_fromBiogrid             = re.search(":(.*)\|BIOGRID:(.*)", splitline[0]).group(2) 
            interactor1_newEntry                = interactor.objects.get_or_create( biogrid_id     = interactor1_fromBiogrid, \
                                                                     species_key    = species1_newEntry)[0] 

            interactor2_fromBiogrid             = re.search(":(.*)\|BIOGRID:(.*)",splitline[1]).group(2)       
            interactor2_newEntry                = interactor.objects.get_or_create( biogrid_id     = interactor2_fromBiogrid, \
                                                                     species_key    = species2_newEntry)[0] 
            
            # 3. Interaction::
            interaction_type_fromBiogrid        = splitline[11]
            interaction_src_db_id_from_file     = splitline[-2]
            method_fromBiogrid_id               = re.search('(.+):"(.+)"', splitline[6]).group(1)
            method_fromBiogrid_id_format        = re.search('(.+):"(.+)"', splitline[6]).group(2)     
            author                              = re.search('(.+)\((\d+)', splitline[7]).group(1)
            year                                = re.search('(.+)\((\d+)', splitline[7]).group(2)     
            Publication_Identifier_fromBiogrid  = re.search('(.+):(.+)', splitline[8]).group(2) 
            Publication_Source_fromBiogrid      = re.search('(.+):(.+)', splitline[8]).group(1) 

        
            interaction_newentry = interaction.objects.get_or_create(   interaction_src_db_id   =   interaction_src_db_id_from_file,
                                                                        interaction_type_id     =   interaction_type_fromBiogrid,
                                                                        method                  =   method_fromBiogrid_id,
                                                                        method_format           =   method_fromBiogrid_id_format,
                                                                        publication_id          =   Publication_Identifier_fromBiogrid,
                                                                        publication_source      =   Publication_Source_fromBiogrid,
                                                                        publication_year        =   year,
                                                                        publication_author      =   author
                                                                        )[0]

            interaction_newentry.interactor_m2m_key.add(interactor1_newEntry, interactor2_newEntry)

    species_map.closefiles()

if __name__ == '__main__':
    biogrid_to_django( os.path.abspath( sys.argv[1] ))
