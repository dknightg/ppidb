#!usr/bin/python -o

import re
import os
import sys
import time

from djangolib import *


def get_true_positive_result(datasets):

    imp_feature_list = ["sgd_features_weight_imputed" , "sgd_features_cai_imputed", "sgd_features_fop_imputed", "sgd_features_gravy_imputed", "sgd_features_weight_imputed"]


    query = hc_interactions.objects.filter( dataset_name__in = datasets ).select_related()

    cai_list        = []
    weight_list     = []
    fop_list        = []
    gravy_list      = []
    length_list     = []

    result_list = [cai_list, weight_list, fop_list, gravy_list, length_list]




    interactor_set= set()



    for hc_int in query:
        interactors = hc_int.interaction_key.interactor_m2m_key.select_related()
        
        if frozenset(list(interactors)) in interactor_set:#
            #print "got one"
            continue

        interactor_set.add(frozenset(list(interactors)))

        cai     = []
        weight  = []
        fop     = []
        gravy   = []
        length  = []

        for i in interactors:

            sgd=i.sgd_features_set.select_related()[0]

            #                                \/ imputed ones ignored
            if sgd.sgd_features_cai_imputed     == False:
                cai.append(     (sgd.sgd_features_cai    )  )
            else:
                cai.append(None)

            if sgd.sgd_features_weight_imputed  == False:
                weight.append(  (sgd.sgd_features_weight )  )
            else:
                cai.append(None)


            if sgd.sgd_features_fop_imputed     == False:
                fop.append(  (sgd.sgd_features_fop )        )
            else:
                fop.append(None)


            if sgd.sgd_features_gravy_imputed   == False:
                gravy.append(  (sgd.sgd_features_gravy )    )
            else:
                gravy.append(None)
            
            if sgd.sgd_features_length_imputed  == False:
                length.append(  (sgd.sgd_features_length )  ) 
            else:
                length.append(None)
        
        interaction_info = [cai, weight, fop, gravy, length]

        for i, result in enumerate(interaction_info):
            if len(result)==2:
                result_list[i].append(result)

        cai_list    = result_list[0]
        weight_list = result_list[1]
        fop_list    = result_list[2]
        gravy_list  = result_list[3]
        length_list = result_list[4]

    return cai_list, weight_list, fop_list, gravy_list, length_list


def get_tp_disorder(datasets, method):

    query = hc_interactions.objects.filter(dataset_name__in = datasets).select_related()
    
    tallyzeros  = []
    tallyones   = []

    hc_interactor_set = set()
    

    print query.count()

    for hc_int in query:
        interactors = hc_int.interaction_key.interactor_m2m_key.select_related()
        hc_interactor_set.add(frozenset(interactors))
    
    for hc_int in hc_interactor_set:

        tallyzero = []
        tallyone  = []

        new_hc_int = list(hc_int)

        if len(hc_int) == 1:
            new_hc_int = [new_hc_int[0], new_hc_int[0]]
    
        for protein in new_hc_int:
            mds =  protein.mobi_disorder_set.select_related()


            for md in mds: 
                if md.mobi_disorder_method == method and md.imputed == False:

                    tallyzero.append(md.mobi_disorder_Tally_Zero)
                    tallyone.append(md.mobi_disorder_Tally_One)
                    break
        
        if (len(tallyzero) == 2) and (len(tallyone) ==2):
            tallyzeros.append(tallyzero)    
            tallyones.append(tallyone)

        #print tallyzeros, tallyones

    return [tallyzeros,tallyones]
    
    


if __name__ == '__main__':
    datasets = ["CCSB-YI1"]
    a= get_tp_disorder(datasets, "disembl-465")
    print a
