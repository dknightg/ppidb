#----------------------------------------------
#   IMPORTS                                 
#----------------------------------------------

import re
import os
import sys

### set up django environment      # import models schema for database PPIbiogrid
from djangolib import *
from PPIbiogrid.models import * 

### random number generator to sample
from random import randint


def get_n_trueNegative_results(n,table,field_list,method):

    table_protein_list = table.objects.filter(mobi_disorder_method = method).select_related()

    resultSet   = set()
    pair_set    = set()
    results_dict= {}

    for field in field_list:
        results_dict[field] = []

    samples  = []
    pair_set = set()

    while len(samples) < n-1:

        a_ind = randint(0, len(table_protein_list)-1)
        b_ind = randint(0, len(table_protein_list)-1)

        pair  = frozenset([a_ind, b_ind])

        if pair in pair_set:
            continue
        else:
            pair_set.add(pair)

        a = table_protein_list[a_ind]
        b = table_protein_list[b_ind]

        # FILTER OUT TP PAIRS:
        if a.interactor_key.interaction_set.count() > 0: # if thsi mobi protein does not interact, ignore 
            a_interactions      = a.interactor_key.interaction_set.all()    # pull out the interactions a is a part of
            a_interactions_set  = set(a_interactions)                       # add to the set of interactions a is a part of

        if b.interactor_key.interaction_set.count() > 0:    
            b_interactions      = b.interactor_key.interaction_set.all()
            b_interactions_set  = set(b_interactions)                       # add the set of interactions b is a part of


        if len(a_interactions_set.intersection(b_interactions_set)) == 0:   # if interaction sets of a and b intersect it means they interact!!
                                                                            #... if they dont intersect then they are true negatives!!
            samples.append([a,b])                                           #... --> so add them to the set of non-interacting pairs


        temp_dict = {}
        flag = True

        for field in field_list:
            
            a_field     = getattr(a,field)
            b_field     = getattr(b,field)

            a_field_imp = getattr(a,field+'_imputed')
            b_field_imp = getattr(b,field+'_imputed')

            # IF NONE --> FILTER OUTFILTER OUT 
            if a_field == None or b_field == None or a_field_imp or b_field_imp:
                flag = False
                break

            temp_dict[field]=([a_field,b_field])

        if flag==False:
            continue

        for field in field_list:
            results_dict[field].append(temp_dict[field])

    return results_dict


#----------------------------------------------
#   MAIN                                    
#----------------------------------------------



if __name__ =="__main__":

    '''NON_INTERACTORS & EXTRACT FEATURES'''

    print 'extracting mobi_features from randomly sampled non-interacting pairs...'

    rd = get_n_trueNegative_results(4000,mobi_disorder,[    'mobi_disorder_Tally_Zero',\

                                                            'mobi_disorder_Tally_One',\

                                                            'mobi_disorder_method'],

                                                            'disembl-465')       

    

    # @@@@@@@@@@@@@@ change samples to 50



    print rd









    for key in rd.keys():

        print key

        for value in rd[key]:

            print value





    # out_file = open('mobi_nonInteracting_pairs.txt','w')

    

    # print 'writing to file...'



    # for feature in rd.keys():

    #   out_file.write('#'+str(feature)+'\n')

    #   for interactor in rd[feature]:

    #       out_file.write(str(interactor)+'\n')



    # out_file.close()


