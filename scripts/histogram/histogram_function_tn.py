import matplotlib.pyplot as plt
import numpy as np
import sys
import sgd_extractor_tn			   # function used to randomly sample and create true negatives dataset
import histogram_function_tp       # use to determine the amount of random samples (n)
from mpl_toolkits.mplot3d import Axes3D
from djangolib import *

fig = plt.figure()
ax = fig.gca(projection = '3d')



#-------------------------------------
# function 
#-------------------------------------


def tn_data(i,datasets,term):

	no_value = []
	termList = []		

#term is that from sgd_features table in django models.py
#options : sgd_features_cai,sgd_features_length, sgd_features_fop
# 		   sgd_features_gravy, sgd_features_length, sgd_features_weight

#-----------------------------------------------------------------------
#specify i , where i is index of 
#term list = cai_list, weight_list, fop_list, gravy_list, length_list
#specify datasets where datasets is for example ["CCSB-YI1"]  from Y2H
#-----------------------------------------------------------------------

 	x = histogram_function_tp.len_dataset(i,datasets)   
	results_dict = sgd_extractor_tn.get_n_trueNegative_results(x,sgd_features,[term])
	for elements in results_dict.values():
		for element in elements:
			if ((element[0] == None) or (element[1] == None)):
				no_value.append(element)
			else:
				termList.append(element)

#continue on to create the histogram
	c = zip(*termList)


	protA= 	c[0]
	protB = c[1]

	x, y = (protA, protB)
	hist, xedges, yedges	= np.histogram2d(x,y, bins = 50)

	xpos, ypos = np.meshgrid(xedges[:-1]+0.25, yedges[:-1]+0.25)

	xpos = xpos.flatten()/float(2)
	ypos = ypos.flatten()/float(2)
	zpos = np.zeros_like(xpos)
	dx = xedges[1] - xedges[0]
	dy = yedges[1] - yedges[0]
	dz = hist.flatten()
	ax.bar3d(xpos, ypos, zpos, dx, dy, dz, color='c', zsort='average')
	plt.xlabel('{} protein A'.format(term))			#must specify term name
	plt.ylabel('{} protein B'.format(term))			##must specify term name

	
	plt.savefig('{}_tn.png'.format(term))     #must specify term name in saved file.
 	plt.show()
 	return

if __name__ == '__main__':
	hist = histogram_plt_tn(int(sys.argv[1]), sys.argv[2], sys.argv[3])		
	# arg 1 = index, arg 2 = dataset, arg 3 = term
	