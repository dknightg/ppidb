#

import scipy.special as special
import numpy as np
import scipy.stats as stats
import sgd_extractor_rand
import tp
from djangolib import *
import matplotlib.pyplot as plt

#flag = True     # prevents colorbar from re-printing

def len_dataset(i,datasets):     #where i is the index of termList to be graphed
    termList2   = []
    term_values =[]
    no_value    =[]

    cai_list, weight_list, fop_list, gravy_list, length_list = tp.get_true_positive_result(datasets)
    termList2 = [cai_list, weight_list, fop_list, gravy_list, length_list]
    for elements in termList2[i]:   
        if ((elements[0] == None) or (elements[1] == None)):
            no_value.append(elements)
        else:
            term_values.append(elements)
        samplesize_tp = len(term_values)
    return samplesize_tp


def t_tester(samplesize_tn, samplesize_tp, feature, datasets, resolution):

    #--------------------------------------------------------------------------------------------------------------------------------
    print '\t'+'extracting tp data from django...'

    sgd_features_cai, sgd_features_weight, sgd_features_fop, sgd_features_gravy, sgd_features_length = tp.get_true_positive_result(datasets)
    #       ^ lists of proteins's term values
    feature_list_tp = [ (float(a[0]), float(a[1])) for a in eval(feature) if (a[0]!=None and a[1] !=None)]
    #       ^ depending on the argument given in the function, one of these terms is picked   ^ this "and" is v important!!
    #                           ^ float() so that we dont get data-types that confuse downstream functions
    feature_list_tp_T = zip(*feature_list_tp)           # @WARNING: this retruns a tuple

    #print feature_list_tp_T

    #--------------------------------------------------------------------------------------------------------------------------------
    print '\t'+'extracting data for rand...'

    results_dict            = sgd_extractor_rand.get_n_trueNegative_results(samplesize_tn ,sgd_features, [feature])    # ??
    feature_list_rand       = results_dict[feature]  # ??
    feature_list_rand       = [ (float(a[0]), float(a[1])) for a in feature_list_rand ]     # converts data into floats!
    #          ^ @@@
    feature_list_rand_T     = zip(*feature_list_rand)

    #print feature_list_rand_T


    #--------------------------------------------------------------------------------------------------------------------------------
    '''@@@@@@@@@@@ \/ really messy '''
    # rename to something less buggy
    for i,dataset in enumerate(datasets):
        if dataset == 'Combined-AP/MS':
            datasets[i] = 'Combined-APMS'

    #--------------------------------------------------------------------------------------------------------------------------------
    print '\t'+'applying t-test --> calculating p values...'

    #iteratively plots each of the histograms above as heatmap::

    #need to make bins consistent between the tps and tns!!! :
    axis_range = list(feature_list_rand_T[0]) + list(feature_list_rand_T[1]) + list(feature_list_tp_T[1]) + list(feature_list_tp_T[0])
    xmin = min(axis_range)     # @WARN: does x correspond to [0] or [1] ??
    xmax = max(axis_range)
    ymin = min(axis_range)
    ymax = max(axis_range)

    # xmin = 0     # @WARN: does x correspond to [0] or [1] ??
    # xmax = 1
    # ymin = 0
    # ymax = 1

    print xmin,xmax,ymin,ymax

    bin_no = 10

    #print list(feature_list_tp_T[0])
    #print list(feature_list_tp_T[1])


#     density_array_tp, xedges, yedges        = np.histogram2d(   np.array(list(feature_list_tp_T[0])),\
#                                                                 np.array(list(feature_list_tp_T[1])),\
#                                                                 bins=(bin_no),\
#                                                                 normed=True,\
#                                                                 range = [[xmin, xmax], [ymin, ymax]])
# #                                                                                                                                       ^ ensures that bins are kept consistent between tp and tn
    count_array_tp, xedges, yedges          = np.histogram2d(   list(feature_list_tp_T[0]),\
                                                                list(feature_list_tp_T[1]),\
                                                                bins=(bin_no),\
                                                                normed=False,\
                                                                range = [[xmin, xmax], [ymin, ymax]])

    total_tp = np.sum(count_array_tp)

    for i,row in enumerate(count_array_tp):
        for j,col in enumerate(row):
            count_array_tp[i,j] = count_array_tp[i,j] / total_tp
    # normalises counts into freqs @WARN: the function above had errors...

    density_array_tp = count_array_tp

    # DEBUGGER FILE:
    #####################################
    out_file = open('debugger_tp.txt','w')
    for row in density_array_tp.tolist():
        out_file.write(str(row)+'\n')
    out_file.close()
    #######################################


    #######################################
    # plot:
    extent = [xmin, xmax, ymin, ymax]
    plt.imshow(density_array_tp, extent=extent, interpolation='nearest',cmap=plt.cm.jet)
    #plt.colorbar()
    # save:
    out_file_name = str('tmap_'+ ''.join(datasets) + '_' + feature + '_tp') # auto-names file
    plt.savefig(out_file_name)                                                  # saves file
    #######################################

    #--------------------------------------------------------------------------------------------------------------------------------

    # tn::
    # density_array_tn, xedges, yedges        = np.histogram2d(   list(feature_list_rand_T[0]),\
    #                                                             list(feature_list_rand_T[1]),\
    #                                                             bins=(bin_no),\
    #                                                             normed=True,\
    #                                                             range = [[xmin, xmax], [ymin, ymax]])

    count_array_tn, xedges, yedges          = np.histogram2d(   list(feature_list_rand_T[0]),\
                                                                list(feature_list_rand_T[1]),\
                                                                bins=(bin_no),\
                                                                normed=False,\
                                                                range = [[xmin, xmax], [ymin, ymax]])

    total_tn = np.sum(count_array_tn)

    for i,row in enumerate(count_array_tn):
        for j,col in enumerate(row):
            count_array_tn[i,j] = count_array_tn[i,j] / total_tn

    density_array_tn = count_array_tn

    # DEBUGGER FILE:
    #####################################
    out_file = open('debugger_tn.txt','w')
    for row in count_array_tn.tolist():
        out_file.write(str(row)+'\n')
    out_file.close()
    #######################################

    #######################################
    # plot:
    extent = [xmin, xmax, ymin, ymax]
    plt.imshow(density_array_tn, extent=extent, interpolation='nearest',cmap=plt.cm.jet)
    #plt.colorbar()
    # save:
    out_file_name = str('tmap_'+ ''.join(datasets) + '_' + feature + '_tn') # auto-names file
    plt.savefig(out_file_name)                                                  # saves file
    #######################################

    #--------------------------------------------------------------------------------------------------------------------------------
    #--------------------------------------------------------------------------------------------------------------------------------
    # denom = sqrt( p^ * ( 1-p^ ) * ( 1/n_1 + 1/n_2 )
    # numer = p_1 - p_2
    # z = numer / denom
    # p^ = (n_1 * p_1) + (n_2 * p_2)

    p_1     = density_array_tp
    n_1     = count_array_tp
    p_2     = density_array_tn
    n_2     = count_array_tn

    # DEBUGGER FILE:
    #####################################
    out_file = open('debugger_4.txt','w')
    for i in [p_1,p_2,n_1,n_2]:
        #print '\n\n'
        for row in i.tolist():
            out_file.write(str(row)+'\n')
    out_file.close()
    #######################################

    # # crude solution to the /0 issue:
    # for array in [p_1,p_2,n_1,n_2]:
    #     for i,row in enumerate(array):
    #         for j,col in enumerate(row):
    #             if array[i,j] == 1:
    #                 continue
    #             if array[i,j] != 'nan':
    #                 array[i,j] = array[i,j] + 0.1
    #                 continue
    #             if array[i,j] == 0:
    #                 array[i,j] = array[i,j] + 0.1
    #                 continue

        # crude solution to the /0 issue:
    for array in [n_1,n_2]:
        for i,row in enumerate(array):
            for j,col in enumerate(row):
                if array[i,j] == 0:
                    #print 'cool'
                    n_1[i,j] = float('NaN')
                    n_2[i,j] = float('NaN')
                    p_1[i,j] = float('NaN')
                    p_2[i,j] = float('NaN')
#                       ^ corresponding 'non-comparables'                    

    
    # for array in [p_1,p_2]:
    #     for i,row in enumerate(array):
    #         for j,col in enumerate(row):
    #             if array[i,j] == 0:
    #                 print 'cool'
    #                 array[i,j] = float('NaN')


    # print p_1
    # print n_1
    # print p_2
    # print n_2

    p_hat   = ( ((n_1 * p_1) + (n_2 * p_2)) / (n_1 + n_2)   )

    # DEBUGGER FILE:
    #####################################
    out_file = open('debugger_5.txt','w')
    for row in p_hat.tolist():
        out_file.write(str(row)+'\n')
    out_file.close()
    #######################################

    numer   = (p_1 - p_2) 
    
    fiddle =( (np.ones(n_1.shape)/n_1)+(np.ones(n_2.shape)/n_2)   )
#                  ^ constants will not do! we need an array of constants to ensure element by element calculations!
#                           ^... matrix of 1s    ^-----------^ these must match

    # DEBUGGER FILE:
    #####################################
    out_file = open('debugger_2.txt','w')
    for row in fiddle.tolist():
        out_file.write(str(row)+'\n')
    out_file.close()
    #######################################

    hatty = ( p_hat*( np.ones(p_hat.shape)- p_hat ) )
#                       ^ matrix of 1s

    # DEBUGGER FILE:
    #####################################
    out_file = open('debugger_3.txt','w')
    for row in hatty.tolist():
        out_file.write(str(row)+'\n')
    out_file.close()
    #######################################

    combo = (hatty*fiddle) 

    denom = np.sqrt( combo  )

    z = numer / denom      # @WARN: for (p_1 - p_2) = as absolutes or?

    p_huh = special.ndtr(z)

    p = (np.ones(n_1.shape)) - p_huh
    #       ^ again notice the 1s

    for i,row in enumerate(p):
        for j,col in enumerate(row):
            if p[i,j] == float('NaN'):
                print 'its a nan!'
                p[i,j] = 1.
            if p[i,j] == 0.:
                print 'its a zero!'
                p[i,j] == 1.

        # DEBUGGER FILE:
    #####################################
    out_file = open('debugger_p.txt','w')
    for row in p.tolist():
        out_file.write(str(row)+'\n')
    out_file.close()
    #######################################
    

    #numer   = (p_1 - p_2) 
    #p_hat   = (n_1*p_1) + (n_2*p_2)

    #z = (p_hat*(1-p_hat)*((1/n_1)+(1/n_2)))

    #z   = (p_1 - p_2) / np.sqrt( (p_hat*(1-p_hat)*((1/n_1)+(1/n_2)))  )      # @WARN: for (p_1 - p_2) = as absolutes or?

    #z   = (p_1 - p_2) / np.sqrt( (p_hat*(1-p_hat)*((1/n_1)+(1/n_2)))  )      # @WARN: for (p_1 - p_2) = as absolutes or?


    # DEBUGGER FILE:
    #####################################
    out_file = open('debugger.txt','w')
    for row in p.tolist():
        out_file.write(str(row)+'\n')
    out_file.close()
    #######################################

    # PLOTTING:
    ######################################
    # xmin = np.amin(z)
    # xmax = np.amax(z)
    # ymin = np.amin(z)
    # ymax = np.amax(z)
    # plot:
    extent = [xmin, xmax, ymin, ymax]
    plt.imshow(p, extent=extent, interpolation='nearest',cmap=plt.cm.jet, vmin=0., vmax=1.0)
    # if flag == True:
    #     plt.colorbar()
    #     flag = False
    # save:
    out_file_name = str('tmap_'+ ''.join(datasets) + '_' + feature + '_difference') # auto-names file
    plt.savefig(out_file_name)                                                      # saves file
    #######################################
    #plt.show()

t_tester(samplesize_tn= 10000, samplesize_tp= 3901, feature= 'sgd_features_cai', datasets= ['s_cerevisiae_ss'], resolution= [10,10])
t_tester(samplesize_tn= 10000, samplesize_tp= 3901, feature= 'sgd_features_weight', datasets= ['s_cerevisiae_ss'], resolution= [10,10])
t_tester(samplesize_tn= 10000, samplesize_tp= 3901, feature= 'sgd_features_length', datasets= ['s_cerevisiae_ss'], resolution= [10,10])
t_tester(samplesize_tn= 10000, samplesize_tp= 3901, feature= 'sgd_features_fop', datasets= ['s_cerevisiae_ss'], resolution= [10,10])
t_tester(samplesize_tn= 10000, samplesize_tp= 3665, feature= 'sgd_features_gravy', datasets= ['s_cerevisiae_ss'], resolution= [10,10])

t_tester(samplesize_tn= 10000, samplesize_tp= 1547, feature= 'sgd_features_cai', datasets= ['Combined-AP/MS'], resolution= [10,10])
t_tester(samplesize_tn= 10000, samplesize_tp= 1547, feature= 'sgd_features_weight', datasets= ['Combined-AP/MS'], resolution= [10,10])
t_tester(samplesize_tn= 10000, samplesize_tp= 1547, feature= 'sgd_features_length', datasets= ['Combined-AP/MS'], resolution= [10,10])
t_tester(samplesize_tn= 10000, samplesize_tp= 1547, feature= 'sgd_features_fop', datasets= ['Combined-AP/MS'], resolution= [10,10])
t_tester(samplesize_tn= 10000, samplesize_tp= 1547, feature= 'sgd_features_gravy', datasets= ['Combined-AP/MS'], resolution= [10,10])

t_tester(samplesize_tn= 10000, samplesize_tp= 1547, feature= 'sgd_features_cai', datasets= ['CCSB-YI1'], resolution= [10,10])
t_tester(samplesize_tn= 10000, samplesize_tp= 1547, feature= 'sgd_features_weight', datasets= ['CCSB-YI1'], resolution= [10,10])
t_tester(samplesize_tn= 10000, samplesize_tp= 1547, feature= 'sgd_features_length', datasets= ['CCSB-YI1'], resolution= [10,10])
t_tester(samplesize_tn= 10000, samplesize_tp= 1547, feature= 'sgd_features_fop', datasets= ['CCSB-YI1'], resolution= [10,10])
t_tester(samplesize_tn= 10000, samplesize_tp= 1547, feature= 'sgd_features_gravy', datasets= ['CCSB-YI1'], resolution= [10,10])
