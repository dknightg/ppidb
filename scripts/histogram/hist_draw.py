import matplotlib.pyplot as plt
import numpy as np
from scipy import stats




def hist_draw(protAtps,protBtps,protAtns,protBtns, dataset, method , xlabel, ylabel):
	fig = plt.figure(1,figsize=(10.,5.))
	ax1 = fig.add_subplot(1,2,1)


	axis_range = list(protAtns) + list(protBtns) + list(protAtps) + list(protBtps)

	### WEIGHT
	xmin = min(axis_range)
	ymin = min (axis_range)

	xmax = max(axis_range)
	ymax = max(axis_range)

	#### WEIGHT
	#xmin = min(protAtps)  - 1000
	#ymin = min (protBtps) - 1000

	#xmax = max(protAtps) + 10000
	#ymax = max(protBtps) + 10000

	#####GRAVY SCORE 
	#xmin = -2.5
	#ymin = -2.5

	#xmax = 2.5
	#ymax = 2.5

	#### LENGTH 
	#xmin = min(protAtps)  
	#ymin = min (protBtps) 

	#xmax = max(protAtps) + 1000
	#ymax = max(protBtps) + 1000

	#### CAI, FOP, DISORDER 
	#xmin = 0 
	#ymin = 0 

	#xmax = 1 
	#ymax = 1

	X, Y = np.mgrid[xmin:xmax:100j, ymin:ymax:100j]
	positions = np.vstack([X.ravel(), Y.ravel()])
		
	kernel = stats.gaussian_kde([protAtps, protBtps])
	Z = np.reshape(kernel(positions).T, X.shape)
	#print Z
	ax1.imshow(np.rot90(Z), cmap=plt.cm.gist_earth_r, extent=[xmin, xmax, ymin, ymax])
	ax1.plot(protAtps, protBtps, 'k.', markersize=1)
	ax1.set_xlim([xmin, xmax])
	ax1.set_ylim([ymin, ymax])

	plt.xlabel(xlabel)			
	plt.ylabel(ylabel)	



	plt.suptitle('{} {}'.format(method,dataset))      
	plt.title('True Positive')

	ax2=fig.add_subplot(1,2,2)

	kernel = stats.gaussian_kde([protAtns, protBtns])
	Z = np.reshape(kernel(positions).T, X.shape)
	ax2.imshow(np.rot90(Z), cmap=plt.cm.gist_earth_r, extent=[xmin, xmax, ymin, ymax])
	ax2.plot(protAtns, protBtns, 'k.', markersize=1)
	ax2.set_xlim([xmin, xmax])
	ax2.set_ylim([ymin, ymax])

	#plt.xlabel('disorderA')			
	#plt.ylabel('disorderB')		

	plt.title('True Negative')

	plt.savefig('{} {}'.format(method,dataset).replace('/', ''))
	#import pdb;pdb.set_trace()
	plt.show()
	return
