from djangolib import *
import tp
import mobi_extractor_tn
import matplotlib.pyplot as plt
import numpy as np
from djangolib import *
from scipy import stats
import hist_draw





#######true postive data gathering

datasets 		= sys.argv[2:]							#CCSB-YI1   dataset name
rd 				= tp.get_tp_disorder(datasets,sys.argv[1])    	#arg 1 is method of calculating disorder
disorder_tp 		= [ [interaction[1][0]/float(interaction[1][0]+interaction[0][0]),interaction[1][1]/float(interaction[0][1]+interaction[1][1])]  for interaction in zip(*rd)]
disorder_tp 	= zip(*disorder_tp)

disorderA_tp 		= disorder_tp[0]
disorderB_tp 		= disorder_tp[1]

num_tp_samples 	= len(disorderA_tp)




####### True Negative data gathering

rd = mobi_extractor_tn.get_n_trueNegative_results(num_tp_samples,mobi_disorder,['mobi_disorder_Tally_Zero',\
							   'mobi_disorder_Tally_One',\
							   'mobi_disorder_method'],
							   sys.argv[1])		

zeros  			= rd["mobi_disorder_Tally_Zero"]
ones 			= rd["mobi_disorder_Tally_One"]

ones_zeros 		= [zeros] + [ones]

disorder_tn 		= [ [interaction[1][0]/float(interaction[1][0]+interaction[0][0]),interaction[1][1]/float(interaction[0][1]+interaction[1][1])]  for interaction in zip(*ones_zeros)]
disorder_tn 		= zip(*disorder_tn)
disorderA_tn 		= disorder_tn[0]
disorderB_tn		= disorder_tn[1]


hist_draw.hist_draw(disorderA_tp,disorderB_tp,disorderA_tn,disorderB_tn, sys.argv[2:],sys.argv[1] , 'disorderA', 'disorderB')   
#arg 1  is method arg 2 is dataset 
