#----------------------------------------------
#   IMPORTS                                    |
#----------------------------------------------

import re
import os
import sys

### set up django environment      # import models schema for database PPIbiogrid
from djangolib import *
from PPIbiogrid.models import * 

### random number generator to sample
from random import randint

#----------------------------------------------
#   FUNCTIONS                                  |
#----------------------------------------------

def get_n_trueNegative_results(n,table,field_list):

    table_protein_list = table.objects.select_related()

    resultSet   = set()
    pair_set    = set()
    results_dict= {}

    for field in field_list:
        results_dict[field] = []

    samples  = []
    pair_set = set()

    while len(samples) < n:

        a_ind = randint(0, len(table_protein_list)-1)
        b_ind = randint(0, len(table_protein_list)-1)

        pair  = frozenset([a_ind, b_ind])

        if pair in pair_set:
            continue
        else:
            pair_set.add(pair)

        a = table_protein_list[a_ind]
        b = table_protein_list[b_ind]

        # FILTER OUT TP PAIRS:
        # if a.interactor_key.interaction_set.count() > 0:
        #   a_interactions      = a.interactor_key.interaction_set.all()
        #   a_interactions_set  = set(a_interactions)

        # if b.interactor_key.interaction_set.count() > 0:
        #   b_interactions      = b.interactor_key.interaction_set.all()
        #   b_interactions_set  = set(b_interactions)
        
        # if len(a_interactions_set.intersection(b_interactions_set)) == 0:
        #   samples.append([a,b])

        temp_dict = {}
        flag = True

        for field in field_list:
            
            a_field     = getattr(a,field)
            b_field     = getattr(b,field)

            a_field_imp = getattr(a,field+'_imputed')
            b_field_imp = getattr(b,field+'_imputed')

            if a_field == None or b_field == None or a_field_imp or b_field_imp:
                flag = False
                break

            temp_dict[field]=([a_field,b_field])

        if flag==False:
            continue

        samples.append([a,b])

        for field in field_list:
            results_dict[field].append(temp_dict[field])

    return results_dict

#----------------------------------------------
#   MAIN                                       |
#----------------------------------------------

if __name__ =="__main__":
    
    '''NON_INTERACTORS & EXTRACT FEATURES'''
    print 'extracting sgd_features from randomly sampled non-interacting pairs...'
    
    rd = get_n_trueNegative_results(5000,sgd_features,[     'sgd_features_cai',\
                                                            'sgd_features_length',\
                                                            'sgd_features_fop',\
                                                            'sgd_features_gravy',\
                                                            'sgd_features_length',\
                                                            'sgd_features_weight'])
    
    
    
    out_file = open('sgd_nonInteracting_pairs.txt','w')
    
    print 'writing to file...'

    for feature in rd.keys():
        out_file.write('#'+str(feature)+'\n')
        for interactor in rd[feature]:
            out_file.write(str(interactor)+'\n')

    out_file.close()





