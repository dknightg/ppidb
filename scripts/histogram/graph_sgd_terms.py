import matplotlib.pyplot as plt
import sys
import numpy as np
import tp 
import sgd_extractor_tn	
from scipy import stats
import hist_draw
from djangolib import *


#-------------------------------------
# function 
#-------------------------------------



def len_dataset(i,datasets):     #where i is the index of termList to be graphed
	termList2 	= []
	term_values =[]
	no_value 	=[]
	x 			= None 

	cai_list, weight_list, fop_list, gravy_list, length_list = tp.get_true_positive_result(datasets)
	termList2 = [cai_list, weight_list, fop_list, gravy_list, length_list]
	for elements in termList2[i]:	
		if ((elements[0] == None) or (elements[1] == None)):
			no_value.append(elements)
		else:
			term_values.append(elements)
		x = len(term_values)
		print x
	return x

if __name__ == '__main__':
	y = len_dataset(4,['CCSB-YI1'])
	print y


'''	


def tp_data(i,datasets): 

	termList3 	= []
	term_values2 = []
	no_value 	=[]

	cai_list, weight_list, fop_list, gravy_list, length_list = tp.get_true_positive_result(datasets)
	termList3 = [cai_list, weight_list, fop_list, gravy_list, length_list]
	for elements in termList3[i]:	
		if ((elements[0] == None) or (elements[1] == None)):
			no_value.append(elements)
		else:
			term_values2.append(elements)

	term_names = ['CAI Score', 'Weight', 'Frequency of Optimal Peptides', 'Gravy Score', 'Length of Protein']

	c = zip(*term_values2)
	
	protAtps =	[]
	protBtps = 	[]

	protA_tp	= 	c[0]
	protB_tp 	= c[1]
	print min(protA_tp)
	print max(protA_tp)
	print min(protB_tp)
	print max(protB_tp)
	for proteins in protA_tp:
		protAtp = float(proteins)
		protAtps.append(protAtp)
	for proteinz in protB_tp:
		protBtp = float(proteinz)
		protBtps.append(protBtp)
		

 	return protAtps, protBtps



def tn_data(i,datasets,term):

	no_value = []
	termList = []		


#term is that from sgd_features table in django models.py
#options : sgd_features_cai,sgd_features_length, sgd_features_fop
# 		   sgd_features_gravy, sgd_features_length, sgd_features_weight

#-----------------------------------------------------------------------
#specify i , where i is index of 
#term list = cai_list, weight_list, fop_list, gravy_list, length_list
#specify datasets where datasets is for example ["CCSB-YI1"]  from Y2H
#-----------------------------------------------------------------------

	x = len_dataset(i,datasets)   
	results_dict = sgd_extractor_tn.get_n_trueNegative_results(x,sgd_features,[term])
	for elements in results_dict.values():
		for element in elements:
			if ((element[0] == None) or (element[1] == None)):
				no_value.append(element)
			else:
				termList.append(element)
#continue on to create the histogram
	c = zip(*termList)

	protBtns = []
	protAtns = []

	protA_tn 	= 	c[0]
	protB_tn	= c[1]
	#print min(protA_tn)
	#print max(protA_tn)
	#print min(protB_tn)
	#print max(protB_tn)
	for proteins in protA_tn:
			protAtn = float(proteins)
			protAtns.append(protAtn)
	for proteinz in protB_tn:
			protBtn = float(proteinz)
			protBtns.append(protBtn)
 	return protAtns, protBtns 
 	

term_names = ['CAI Score', 'Weight', 'Frequency of Optimal Peptides', 'Gravy Score', 'Length of Protein']
TERM = term_names[int(sys.argv[1])]
field_names_sgd = ['sgd_features_cai', 'sgd_features_weight', 'sgd_features_fop', 'sgd_features_gravy', 'sgd_features_length']
field_name = field_names_sgd[int(sys.argv[1])]



protAtns , protBtns  = tn_data(int(sys.argv[1]), sys.argv[2:], field_name)    #argv 1 is the index of cai_list etc. 
protAtps , protBtps	 = tp_data(int(sys.argv[1]), sys.argv[2:])					#argv2 is dataset name




hist_draw.hist_draw(protAtps , protBtps,protAtns , protBtns , sys.argv[2:],TERM , 'ProtA', 'ProtB')
'''