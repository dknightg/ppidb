#!usr/bin/python 

#---------------
# sgd_annotater |
#---------------

import re
import sys
import time

# set up django environment      # import models schema for database PPIbiogrid
from common.djangolib import *
from django.core.exceptions import ObjectDoesNotExist

#sys.path.append('../pickleCheck')

import common.idmapper as idmapper

def sgd_to_django( datapath_sgd , datapath_idmapper, startingLine = 1 ):
#                      ^ this is assigned in djangolib.py <-- config.cfg  }} must specify in the meta_parser }} different for each box
    """
    Description 

    1. Maps biogrid ids of proteins (which the database understands) 
    to sgd ids (which the sgd file contains).

    2. Uses biogrid-sgd id map to attach features of proteins present 
    in the sgd file onto interacting proteins present in the database.

    Arguments      

    datapath_sgd        the path within which the sgd protein features
                        data is stored.
                        (Type=String)

    datapath_idmapper   Datapath for the idmapper module.
                        (Type=String)

    startingLine        The line of the datafile which the function
                        begins parsing from. This defaults to 1.
                        (Type=Integer)

    """

    def conv_or_none( entry, outputType ):
        """

        Description

        converts the object given to the "entry" argument into the 
        data-type given in the "outputType" argument.

        Arguments

        entry       an object whose data-type is to be converted
                    (Type=Any)

        outputType  the type in which the object passed to the "entry"
                    argument is to be converted to
                    (Type=Type)

        """

        try:
            return outputType(entry)
        except ValueError:
            return None

    # Opens sgd data file:
    in_file = open( datapath_sgd )

    # generates i_map dict
    print '\t\tgenerating sgd2biogrid map...'
    
    in_filename = os.path.basename(datapath_sgd)
    # ^ specifies which data_file to name the pickle after

    mappings =  idmapper.bgid_to_dbid_dict( 'SGD', datapath_idmapper, bg_id_2_db_id = False) 
    #                                                                                                                       ^ values:keys direction

    # Initate progress bar:
    #---------------------------------
    t0              = time.clock()            # starts the timer -> can check total elapsed time
    lineCount_skip  = 0
    lineCount       = 0
    #---------------------------------

    # loop through biogrid_id list, id by id:
    print 'Annotating proteins with sgd_features...'
    while True:

        in_line = in_file.readline() # reads the next line, one line per loop
        if in_line == "":
            break

        # LINE SKIPPING::
        #------------------------------------------------------------------------------------------------------------------------------------------------
        lineCount_skip += 1                    # number of lines counted   
        if (lineCount_skip - 1) < startingLine:  # Skips until reaching line specified by "startingLine" arg @1
        #         ^    @ WARN: '-1' --> since the +1 to count does not start until this block is run => we must reference to the past
            continue

        #------------------------------------------------------------------------------------------------------------------------------
        # LOADING BAR::
        elapsedTime     = int(time.clock() - t0)                        # t is wall seconds elapsed (floating point)
        lineCount       += 1
        percentageLeft  = int( ( (6717 - (lineCount + startingLine) )*100 )/6717. )     # percentage of job left
        rate            = int( lineCount / (elapsedTime + 0.1) ) + 0.1                    # total lines per time elapsed
                #                                          ^ prevent /0 err    
        eta             = int( (6717 - (lineCount + startingLine) ) / rate)
        # Prints every 5 secs: 
        if elapsedTime%5 < 1.:  # print ever 5 seconds to prevent slow-down
            print("Remaining %",percentageLeft,"||   lines done:",lineCount+startingLine,"||   Seconds elapsed:",elapsedTime,"||   Rate:",rate,"||   ETA:",eta) #progress bar: percentage left to complete
        #------------------------------------------------------------------------------------------------------------------------------s

        splitline               = in_line.split('\t')

        sgd_id_fromSgdfile      = splitline[1]                      # SGDID
        sgd_weight_fromSgdfile  = conv_or_none(splitline[2],float)  # MOLECULAR WEIGHT (in Daltons)
        sgd_cai_fromSgdfile     = conv_or_none(splitline[4],float)  # CAI (Codon Adaptation Index)
        sgd_fop_fromSgdfile     = conv_or_none(splitline[29],float) # FOP SCORE (Frequency of Optimal Codons)
        sgd_gravy_fromSgdfile   = conv_or_none(splitline[30],float) # GRAVY SCORE (Hydropathicity of Protein)
        sgd_length_fromSgdfile  = conv_or_none(splitline[5],int)    # PROTEIN LENGTH

        try:
            biogrid_id_fromMappings = mappings[sgd_id_fromSgdfile]
            try:        
                sgd_features_newEntry = sgd_features.objects.get_or_create( sgd_features_weight = sgd_weight_fromSgdfile, \
                                                                            sgd_features_cai    = sgd_cai_fromSgdfile, \
                                                                            sgd_features_fop    = sgd_fop_fromSgdfile, \
                                                                            sgd_features_gravy  = sgd_gravy_fromSgdfile, \
                                                                            sgd_features_length = sgd_length_fromSgdfile, \
                                                                            interactor_key      = interactor.objects.get(biogrid_id = biogrid_id_fromMappings)     )[0]
                            #                                              ^the biogrid id that needs to have the features linked to
            except ObjectDoesNotExist:
                continue
        except KeyError:
                #print('mooooo')
                continue

if __name__ == '__main__':
    sgd_to_django( os.path.asbpath( sys.argv[1] ), os.path.abspath( sys.argv[2] ), startingLine = 0 ) 

    """

    Columns in protein_properties.tab:

    FEATURE (ORF name)
    SGDID
    MOLECULAR WEIGHT (in Daltons)
    PI
    CAI (Codon Adaptation Index)
    PROTEIN LENGTH
    N TERM SEQ
    C TERM SEQ
    CODON BIAS
    ALA
    ARG
    ASN
    ASP
    CYS
    GLN
    GLU
    GLY
    HIS
    ILE
    LEU
    LYS
    MET
    PHE
    PRO
    SER
    THR
    TRP
    TYR
    VAL
    FOP SCORE (Frequency of Optimal Codons)
    GRAVY SCORE (Hydropathicity of Protein)
    AROMATICITY SCORE (Frequency of aromatic amino acids: Phe, Tyr, Trp)
    Feature type (ORF classification: Verified, Uncharacterized, Dubious)

    """

